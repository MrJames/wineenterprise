/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mailing;

/**
 *
 * @author James
 */
public class StaffActivateEmail {
    public String link;
    public String username;
    public String password;
    public static String subject = "WineExpress - Admin Account activation";

    public String getContent() {
        return ("Hello, \n\n"
                + "Our Admin has created an account for you! \n\nPlease proceed to activate your account using this link: \n"
                + link + "\n\n"
                + "Your Username and Password are as following: \n\n"
                + "Username: " + username + "\n"
                + "Password: " + password + "\n\n"
                + "Please proceed to change your password as soon as possible! \n\n"
                + "If this email means nothing to you. Somebody could have entered your email address deliberately or accidentally. \n\n"
                + "Thanks and Best regards, \n"
                + "WineExpress");
    }
}
