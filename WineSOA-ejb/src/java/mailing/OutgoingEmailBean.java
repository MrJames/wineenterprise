/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mailing;

import utilities.Constants;
import java.util.Properties;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author James
 */
@MessageDriven(mappedName = "jms/IS4227OutgoingEmailQueue", activationConfig = {
    @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class OutgoingEmailBean implements MessageListener {
    //Variables

    private String sender;
    private String recipient;
    private String subject;
    private String content;
    private int msgType;

    public OutgoingEmailBean() {
    }

    @Override
    public void onMessage(Message message) {
        MapMessage msg = (MapMessage) message;
        try {
            System.out.println("OutgoingEmailBean: Receive a message");
            msgType   = msg.getInt("msgType");
            recipient = msg.getString("recipient");
            System.out.println("OutgoingEmailBean: send email to " + recipient);
            //Based on type of message, decide the content and subject of the email
            switch (msgType) {
                case Constants.EMAIL_MEMBER_ACTIVATION:
                    ActivateEmail ae = new ActivateEmail();
                    subject = ActivateEmail.subject;
                    ae.link = msg.getString("link");
                    content = ae.getContent();
                    break;
                case Constants.EMAIL_FORGOT:
                    ForgotEmail fe = new ForgotEmail();
                    subject = ForgotEmail.subject;
                    fe.password = msg.getString("str1");
                    content = fe.getContent();
                    break;
                case Constants.EMAIL_STAFF_ACTIVATION:
                    StaffActivateEmail sae = new StaffActivateEmail();
                    subject = StaffActivateEmail.subject;
                    sae.link = msg.getString("link");
                    sae.username = msg.getString("str1");
                    sae.password = msg.getString("str2");
                    content = sae.getContent();
                    break;
                case Constants.EMAIL_WELCOME:
                    WelcomeEmail welcomeEmail = new WelcomeEmail();
                    subject = WelcomeEmail.subject;
                    content = welcomeEmail.getContent();
                    break;
                case Constants.EMAIL_ENQUIRY:
                    ContactEmail ce = new ContactEmail();
                    subject    = ContactEmail.subject + msg.getString("str3");
                    ce.name    = msg.getString("str1");
                    ce.email   = msg.getString("str2");
                    ce.enquiry = msg.getString("str4");
                    content    = ce.getContent();
                    sender     = ce.email;
                    break;
                case Constants.EMAIL_ENQUIRY_AUTO_REPLY:
                    ContactAutoReplyEmail care = new ContactAutoReplyEmail();
                    subject      = ContactEmail.subject + msg.getString("str1");
                    care.enquirySubject = msg.getString("str1");
                    care.enquiry        = msg.getString("str2");
                    content      = care.getContent();
                    break;
            }

            //Set the properties of mail server
            Properties props = System.getProperties();
            props.put("mail.host", "smtp.gmail.com");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");

            IS4227SMTPAuthenticator authenticator = new IS4227SMTPAuthenticator(Constants.SYSTEM_MAILBOX_ACCOUNT, Constants.SYSTEM_MAILBOX_PASSWORD);

            Session session = Session.getInstance(props, authenticator);
            MimeMessage mail = new MimeMessage(session);
            if (sender == null) mail.setFrom(new InternetAddress(Constants.SYSTEM_MAILBOX_ACCOUNT));
            else mail.setFrom(new InternetAddress(sender));
            mail.setRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(recipient));
            mail.setSubject(subject);
            mail.setText(content);

            //Send the message
            Transport.send(mail);

        } catch (MessagingException ex) {
            System.out.println("OutgoingEmailBean - MessagingException: " + ex.toString());

        } catch (JMSException ex) {
            System.out.println("OutgoingEmailBean - JMSException: " + ex.toString());

        }
    }
}
