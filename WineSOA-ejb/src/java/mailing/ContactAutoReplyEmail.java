/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mailing;

/**
 *
 * @author JamesTran
 */
public class ContactAutoReplyEmail {
    public static String subject = "WineExpress - Enquiry - ";

    public String enquirySubject;
    public String enquiry;
    
    public String getContent() {
        return ("Hello, \n\n"
                + "This is an auto-generated email. Please do NOT reply! \n\n"
                + "You have submitted the following enquiry:\n"
                + "- Subject: " + enquirySubject + "\n"
                + "- Enquiry: \n" + enquiry + "\n\n"
                + "Our admins will assist you shortly. \n\n"
                + "Thanks and Best regards, \n"
                + "WineExpress");
    }
}
