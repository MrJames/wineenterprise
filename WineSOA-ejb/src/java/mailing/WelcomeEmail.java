/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mailing;

/**
 *
 * @author JamesTran
 */
public class WelcomeEmail {
    public static String subject = "WineExpress - Welcome message";

    public String getContent() {
        return ("Hello, \n\n"
                + "Thank you for registering with WineExpress! \n\n We wish you a juiceful journey with us.\n\n"
                + "If this email means nothing to you. Somebody could have entered your email address deliberately or accidentally. \n\n"
                + "Thanks and Best regards, \n"
                + "WineExpress");
    }
}
