/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mailing;

/**
 *
 * @author James
 */
public class ActivateEmail {
    public String link;
    public static String subject = "WineExpress - Account Activation";

    public String getContent() {
        return ("Hi, \n\n"
                + "Thank you for registering with WineExpress! \n\nPlease proceed to activate your account using this link: \n"
                + link + "\n\n"
                + "Alternatively, you can copy and paste the above link in your browser address bar. \n\n"
                + "If this email means nothing to you, somebody could have entered your email address deliberately or accidentally. \n\n"
                + "Thanks and Best regards, \n"
                + "WineExpress");
    }
}
