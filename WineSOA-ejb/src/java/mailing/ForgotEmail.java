/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mailing;

/**
 *
 * @author James
 */
public class ForgotEmail {
    public static String subject = "WineExpress - Account recovery";
    public String password = "";


    public String getContent() {
        return ("Hello, \n\n"
                +"We have prepared the following new password for you: "
                + password + "\n\n"
                + "Please proceed to change your password as soon as possible. \n\n"
                + "If you don't know why you received this email. Somebody could have tried to get your account token deliberately or accidentally. \n\n"
                + "Thanks and Best regards, \n"
                + "WineExpress");
    }
}
