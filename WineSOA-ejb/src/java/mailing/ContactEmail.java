/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mailing;

/**
 *
 * @author JamesTran
 */
public class ContactEmail {
    public static String subject = "WineExpress - Enquiry - ";

    public String name;
    public String email;
    public String enquiry;
    
    public String getContent() {
        return ("Hello, \n\n"
                + "The following user has submitted an enquiry:\n"
                + "- Name: " + name + "\n"
                + "- Email: " + email + "\n\n"
                + "The enquiry is as following:\n"
                + enquiry + "\n\n"
                + "Thanks and Best regards, \n"
                + "WineExpress");
    }
}
