/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mailing;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 *
 * @author JamesTran
 */
public class IS4227SMTPAuthenticator extends Authenticator {
    private String username;
    private String password;
    
    public IS4227SMTPAuthenticator(String username, String password) {
        this.username = username;
        this.password = password;
    }
    
    @Override
    public PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(username, password);
    }
}
