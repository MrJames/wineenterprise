/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entityStates;

import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author user
 */
public class ProductState implements Serializable {
    private Long    id;
    private String  name;
    private String  description;
    private String  photo;
    private Double  cost;
    private Double  price;
    private Integer type;
    private Integer quantity;
    private Integer totalRating;
    private Integer numberOfReviewers;
    private boolean deleted;
    private Long    createdDate;
    private Set<CategoryState>     categories;
    private List<ProductInfoState> productInfo;
    private List<ReviewState>      reviews;

    public ProductState() {}
    
    public void addCategory(CategoryState cs) {
        this.categories.add(cs);
    }

    public ProductState(Long id, String name, Double price) {
        this.id    = id;
        this.name  = name;
        this.price = price;
    }

    public ProductState(Long id, String name, String description, String photo, Double cost, Double price, Integer type, Integer quantity, Integer totalRating, Integer numberOfReviewers, Long createdDate) {
        this.id                = id;
        this.name              = name;
        this.description       = description;
        this.photo             = photo;
        this.cost              = cost;
        this.price             = price;
        this.type              = type;
        this.quantity          = quantity;
        this.totalRating       = totalRating;
        this.numberOfReviewers = numberOfReviewers;
        this.createdDate       = createdDate;
        this.categories        = new HashSet();
    }
    
    // Getters and Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getTotalRating() {
        return totalRating;
    }

    public void setTotalRating(Integer totalRating) {
        this.totalRating = totalRating;
    }

    public Integer getNumberOfReviewers() {
        return numberOfReviewers;
    }

    public void setNumberOfReviewers(Integer numberOfReviewers) {
        this.numberOfReviewers = numberOfReviewers;
    }

    public Set<CategoryState> getCategories() {
        return categories;
    }

    public void setCategories(Set<CategoryState> categories) {
        this.categories = categories;
    }

    public List<ProductInfoState> getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(List<ProductInfoState> productInfo) {
        this.productInfo = productInfo;
    }

    public List<ReviewState> getReviews() {
        return reviews;
    }

    public void setReviews(List<ReviewState> reviews) {
        this.reviews = reviews;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }
}
