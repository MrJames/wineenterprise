/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entityStates;

import entityStates.CategoryState;
import entities.Category;

/**
 *
 * @author stringtheory
 */
public class CategoryStateEntityConverter {

    public static CategoryState converToState(Category c) {
        try {
            CategoryState state = new CategoryState();
            state.setCreatedDate(c.getCreatedDate());
            state.setDeleted(c.isDeleted());
            state.setId(c.getId());
            state.setName(c.getName());
            state.setType(c.getType());
            return state;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Category converToCategoryEntity(CategoryState state) {
        try {
            Category category = new Category();
            category.setCreatedDate(state.getCreatedDate());
            category.setDeleted(state.isDeleted());
            category.setId(state.getId());
            category.setName(state.getName());
            category.setType(state.getType());
            return category;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
