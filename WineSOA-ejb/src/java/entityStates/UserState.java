/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entityStates;

import java.io.Serializable;

/**
 *
 * @author JamesTran
 */
public class UserState implements Serializable {
    private String email;
    private String firstName;
    private String lastName;
    private String gender;
    private String creditCard;
    private String countryCode;
    private String phone;
    private String billingAddress1;
    private String billingAddress2;
    private String billingCountry;
    private String billingZipCode;
    private String shippingAddress1;
    private String shippingAddress2;
    private String shippingCountry;
    private String shippingZipCode;
    private Long   createdDate;
    private boolean activated;
    private boolean deleted;
    private boolean blocked;
    private boolean isAdmin;

    public UserState() {}

    public UserState(String email, String countryCode, String phone, String billingAddress1, String billingAddress2, String billingCountry, String billingZipCode, String shippingAddress1, String shippingAddress2, String shippingCountry, String shippingZipCode) {
        this.email            = email;
        this.countryCode      = countryCode;
        this.phone            = phone;
        this.billingAddress1  = billingAddress1;
        this.billingAddress2  = billingAddress2;
        this.billingCountry  = billingCountry;
        this.billingZipCode   = billingZipCode;
        this.shippingAddress1 = shippingAddress1;
        this.shippingAddress2 = shippingAddress2;
        this.shippingCountry  = shippingCountry;
        this.shippingZipCode  = shippingZipCode;
    }

    public UserState(String email, String firstName, String lastName, String gender, boolean isAdmin) {
        this.email     = email;
        this.firstName = firstName;
        this.lastName  = lastName;
        this.gender    = gender;
        this.isAdmin   = isAdmin;
    }

    public UserState(String email, String firstName, String lastName, String gender, boolean activated, boolean deleted, boolean blocked, boolean isAdmin) {
        this.email     = email;
        this.firstName = firstName;
        this.lastName  = lastName;
        this.gender    = gender;
        this.activated = activated;
        this.deleted   = deleted;
        this.blocked   = blocked;
        this.isAdmin   = isAdmin;
    }
    
    // Getters and Setters
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBillingCountry() {
        return billingCountry;
    }

    public void setBillingCountry(String billingCountry) {
        this.billingCountry = billingCountry;
    }

    public String getShippingCountry() {
        return shippingCountry;
    }

    public void setShippingCountry(String shippingCountry) {
        this.shippingCountry = shippingCountry;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBillingAddress1() {
        return billingAddress1;
    }

    public void setBillingAddress1(String billingAddress1) {
        this.billingAddress1 = billingAddress1;
    }

    public String getBillingAddress2() {
        return billingAddress2;
    }

    public void setBillingAddress2(String billingAddress2) {
        this.billingAddress2 = billingAddress2;
    }

    public String getShippingAddress1() {
        return shippingAddress1;
    }

    public void setShippingAddress1(String shippingAddress1) {
        this.shippingAddress1 = shippingAddress1;
    }

    public String getShippingAddress2() {
        return shippingAddress2;
    }

    public void setShippingAddress2(String shippingAddress2) {
        this.shippingAddress2 = shippingAddress2;
    }

    public String getBillingZipCode() {
        return billingZipCode;
    }

    public void setBillingZipCode(String billingZipCode) {
        this.billingZipCode = billingZipCode;
    }

    public String getShippingZipCode() {
        return shippingZipCode;
    }

    public void setShippingZipCode(String shippingZipCode) {
        this.shippingZipCode = shippingZipCode;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }
}
