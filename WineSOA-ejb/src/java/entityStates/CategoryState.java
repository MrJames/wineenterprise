/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entityStates;

import java.io.Serializable;

/**
 *
 * @author user
 */
public class CategoryState implements Serializable{
    private Long    id;
    private String  name;
    private Integer type;
    private Long    createdDate;
    private boolean deleted;
    
    public CategoryState() {}
    
    public CategoryState(Long id, String name, Integer type, Long createdDate) {
        this.id          = id;
        this.name        = name;
        this.type        = type;
        this.createdDate = createdDate;
    }

    public CategoryState(Long id, String name, Integer type) {
        this.id   = id;
        this.name = name;
        this.type = type;
    }

    // Getters and Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
    
}
