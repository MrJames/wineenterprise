/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entityStates;

import entityStates.ProductState;
import entities.Product;

/**
 *
 * @author user
 */
public class StateEntityConverter {
     public static ProductState createProductState(Product p){
        ProductState ps = new ProductState();
        ps.setDeleted(p.isDeleted());
        ps.setId(p.getId());
        ps.setName(p.getName());
        ps.setDescription(p.getDescription());
        ps.setPhoto( p.getPhoto());
        ps.setCost(p.getCost());
        ps.setType(p.getType());
        ps.setQuantity( p.getQuantity());
        ps.setTotalRating(p.getTotalRating());
        ps.setNumberOfReviewers(p.getNumberOfReviewers());
        ps.setCreatedDate(p.getCreatedDate());
      double price=p.getInfo(System.currentTimeMillis()).getPrice();
        ps.setPrice(price);
        return ps;
//        this.price = p.getInfos().price;
//        this.startDate = startDate;
//        this.endDate = endDate;
    }
     
     
    public static Product createProduct(ProductState newP) {
        Product p = new Product();
        
        if (newP.getName() != null) {
            p.setName(newP.getName());
        }
        
        if (newP.getCost() != null) {
            p.setCost (newP.getCost());
        }
        
        if (newP.getDescription()!= null) {
            p.setDescription (newP.getDescription());
        }
        
        if (newP.getPhoto()!= null) {
            p.setPhoto (newP.getPhoto());
        }
        
        if (newP.getQuantity()!= null) {
            p.setQuantity(newP.getQuantity());
        } 
        
        if (newP.getType()!= null) {
            p.setType(newP.getType());
        } 
          
        System.out.println("this is in Product entity");
        return p;
    }

}
