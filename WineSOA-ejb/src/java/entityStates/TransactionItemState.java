/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entityStates;

import java.io.Serializable;

/**
 *
 * @author JamesTran
 */
public class TransactionItemState implements Serializable {
    private Long         id;
    private int          quantity;
    private ProductState product;

    public TransactionItemState(Long id, int quantity, ProductState product) {
        this.id       = id;
        this.quantity = quantity;
        this.product  = product;
    }
    
    // Getters and Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public ProductState getProduct() {
        return product;
    }

    public void setProduct(ProductState product) {
        this.product = product;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TransactionItemState other = (TransactionItemState) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
}
