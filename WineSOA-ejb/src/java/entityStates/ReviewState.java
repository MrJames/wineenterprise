/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entityStates;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author user
 */
public class ReviewState implements Serializable{
    private Long    id;
    private String  reviewer;
    private String  comment;
    private Integer rating;
    private Long    createdDate;
   
    private Set <String> likers = new HashSet<String>();
  
    private Set <String> dislikers = new HashSet<String>();
    
    private TransactionsState transactionState;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReviewer() {
        return reviewer;
    }

    public void setReviewer(String reviewer) {
        this.reviewer = reviewer;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public Set<String> getLikers() {
        return likers;
    }

    public void setLikers(Set<String> likers) {
        this.likers = likers;
    }

    public Set<String> getDislikers() {
        return dislikers;
    }

    public void setDislikers(Set<String> dislikers) {
        this.dislikers = dislikers;
    }

    public TransactionsState getTransactionState() {
        return transactionState;
    }

    public void setTransactionState(TransactionsState transactionState) {
        this.transactionState = transactionState;
    }
    
}
