/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import com.google.gson.Gson;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import utilities.email.Email;
import utilities.email.EmailConstants;
import utilities.email.EmailHandler;

/**
 *
 * @author JamesTran
 */
@WebService(serviceName = "EmailWebService")
@Stateless()
public class EmailWebService {
    private EmailHandler emailHandler;
    private Email email;

    @PostConstruct
    public void init() {
        emailHandler = new EmailHandler();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "autoReply")
    public boolean autoReply(@WebParam(name = "emailAddress") String emailAddress) {
        System.out.println("Web Service: EmailWebService.autoReply() is called");
        //TODO write your implementation code here:
        if (emailAddress.isEmpty()) {
            return false;
        } else {
            ArrayList<String> to = new ArrayList<String>();
            to.add(emailAddress);
            email = new Email();
            email.setSubject(EmailConstants.AUTOREPLY_SUBJECT);
            email.setToList(to);
            email.setContent(EmailConstants.AUTOREPLY_CONTENT);
            emailHandler.sendEmail(email);
            return true;
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "receiveEmail")
    public boolean receiveEmail(@WebParam(name = "emailObject") String emailObject) {
        System.out.println("Web Service: EmailWebService.receiveEmail() is called");
        // De-serialize 
        Email email = new Gson().fromJson(emailObject, Email.class);
        if (email.isEmpty()) {
            return false;
        } else {
            email.getToList().add(EmailConstants.MAILBOX_ACCOUNT);
            emailHandler.sendEmail(email);
            return true;
        }
    }
}
