/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import com.google.gson.Gson;
import entities.Category;
import entities.Product;
import entities.TransactionItem;
import entities.Transactions;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.joda.time.DateTime;
import org.joda.time.Period;

/**
 *
 * @author JamesTran
 */
@WebService(serviceName = "ReportServices")
@Stateless()
public class ReportServices {

    @PersistenceContext
    private EntityManager em;
    
    // Constants
    final long MS_ANNUAL  = Long.parseLong("31536000000");
    final long MS_MONTHLY = Long.parseLong("2628000000");
    final long MS_WEEKLY  = Long.parseLong("604800000");
    final long MS_DAILY   = Long.parseLong("86400000");

    final int INTERVAL_YEAR  = 1;
    final int INTERVAL_MONTH = 2;
    final int INTERVAL_WEEK  = 3;
    final int INTERVAL_DAY   = 4;
    
    private Period getIntervalDuration(int intervalType) {
        switch (intervalType) {
            case INTERVAL_YEAR:
                return Period.years(1);
            case INTERVAL_MONTH:
                return Period.months(1);
            case INTERVAL_WEEK:
                return Period.weeks(1);
            case INTERVAL_DAY:
                return Period.days(1);
            default:
                return null;
        }
    }
    
    private String getDatePattern(int intervalType) {
        switch (intervalType) {
            case INTERVAL_YEAR:
                return "MMM-yy";
            default:
                return "dd-MMM";
        }
    }
    
    @WebMethod(operationName = "generateProfitReport")
    public String generateProfitReport(@WebParam(name = "start") long start, @WebParam(name = "end") long end, @WebParam(name = "intervalType") int intervalType, @WebParam(name = "brand") Long brand, @WebParam(name = "region") Long region, @WebParam(name = "flavor") Long flavor) {
        Map<String, Double> result = new LinkedHashMap();
        
        // Calculate interval duration
        Period intervalDuration = this.getIntervalDuration(intervalType);
        String pattern          = this.getDatePattern(intervalType);
        
        // Calculate total revenue for each interval
        Query q = em.createQuery("SELECT T FROM Transactions T");
        List<Transactions> transactions = q.getResultList();
        
        DateTime intervalEnd = new DateTime(start).plus(intervalDuration);
        while (intervalEnd.getMillis() <= end) {
            double totalProfit = 0;
            
            Iterator<Transactions> iter = transactions.iterator();
            while (iter.hasNext()) {
                Transactions   t = iter.next();
                long createdDate = t.getCreatedDate();
                
                if (createdDate >= start && createdDate <= intervalEnd.getMillis()) {
                    // Calculate profit of this transaction
                    double profit = 0;
                    
                    for (TransactionItem item : t.getTransactionItems()) {
                        Product p = item.getProduct();
                        
                        if (brand != null && !p.getCategories().contains(new Category(brand))) continue;
                        
                        if (region != null && !p.getCategories().contains(new Category(region))) continue;
                        
                        if (flavor != null && !p.getCategories().contains(new Category(flavor))) continue;
                        
                        double profitPerItem = p.getInfo(createdDate).getPrice() - p.getCost();
                        profit += item.getQuantity() * profitPerItem;
                    }
                    
                    // Add to total profit
                    totalProfit += profit;
                    iter.remove();
                }
            }
            
            // Prepare for next iteration
            result.put(new DateTime(start).toString(pattern), totalProfit);
            start       = intervalEnd.getMillis();
            intervalEnd = intervalEnd.plus(intervalDuration);
        }
        
        return new Gson().toJson(result);
    }
    
    @WebMethod(operationName = "generateQuantityReport")
    public String generateQuantityReport(@WebParam(name = "start") long start, @WebParam(name = "end") long end, @WebParam(name = "intervalType") int intervalType, @WebParam(name = "brand") Long brand, @WebParam(name = "region") Long region, @WebParam(name = "flavor") Long flavor) {
        Map<String, Double> result = new LinkedHashMap();
        
        // Calculate interval duration
        Period intervalDuration = this.getIntervalDuration(intervalType);
        String pattern          = this.getDatePattern(intervalType);
        
        // Calculate total revenue for each interval
        Query q = em.createQuery("SELECT T FROM Transactions T");
        List<Transactions> transactions = q.getResultList();
        
        DateTime intervalEnd = new DateTime(start).plus(intervalDuration);
        while (intervalEnd.getMillis() <= end) {
            double totalQuantity = 0;
            
            Iterator<Transactions> iter = transactions.iterator();
            while (iter.hasNext()) {
                Transactions   t = iter.next();
                long createdDate = t.getCreatedDate();
                
                if (createdDate >= start && createdDate <= intervalEnd.getMillis()) {
                    for (TransactionItem item : t.getTransactionItems()) {
                        Product p = item.getProduct();
                        
                        if (brand != null && !p.getCategories().contains(new Category(brand))) continue;
                        
                        if (region != null && !p.getCategories().contains(new Category(region))) continue;
                        
                        if (flavor != null && !p.getCategories().contains(new Category(flavor))) continue;
                        
                        totalQuantity += item.getQuantity();
                    }
                    
                    iter.remove();
                }
            }
            
            // Prepare for next iteration
            result.put(new DateTime(start).toString(pattern), totalQuantity);
            start       = intervalEnd.getMillis();
            intervalEnd = intervalEnd.plus(intervalDuration);
        }
        
        return new Gson().toJson(result);
    }
    
    @WebMethod(operationName = "generateCostReport")
    public String generateCostReport(@WebParam(name = "start") long start, @WebParam(name = "end") long end, @WebParam(name = "intervalType") int intervalType, @WebParam(name = "brand") Long brand, @WebParam(name = "region") Long region, @WebParam(name = "flavor") Long flavor) {
        Map<String, Double> result = new LinkedHashMap();
        
        // Calculate interval duration
        Period intervalDuration = this.getIntervalDuration(intervalType);
        String pattern          = this.getDatePattern(intervalType);
        
        // Calculate total revenue for each interval
        Query q = em.createQuery("SELECT T FROM Transactions T");
        List<Transactions> transactions = q.getResultList();
        
        DateTime intervalEnd = new DateTime(start).plus(intervalDuration);
        while (intervalEnd.getMillis() <= end) {
            double totalCost = 0;
            
            Iterator<Transactions> iter = transactions.iterator();
            while (iter.hasNext()) {
                Transactions   t = iter.next();
                long createdDate = t.getCreatedDate();
                
                if (createdDate >= start && createdDate <= intervalEnd.getMillis()) {
                    for (TransactionItem item : t.getTransactionItems()) {
                        Product p = item.getProduct();
                        
                        if (brand != null && !p.getCategories().contains(new Category(brand))) continue;
                        
                        if (region != null && !p.getCategories().contains(new Category(region))) continue;
                        
                        if (flavor != null && !p.getCategories().contains(new Category(flavor))) continue;
                        
                        totalCost += item.getQuantity() * p.getCost();
                    }
                    
                    iter.remove();
                }
            }
            
            // Prepare for next iteration
            result.put(new DateTime(start).toString(pattern), totalCost);
            start       = intervalEnd.getMillis();
            intervalEnd = intervalEnd.plus(intervalDuration);
        }
        
        return new Gson().toJson(result);
    }
    
    @WebMethod(operationName = "generateRevenueReport")
    public String generateRevenueReport(@WebParam(name = "start") long start, @WebParam(name = "end") long end, @WebParam(name = "intervalType") int intervalType, @WebParam(name = "brand") Long brand, @WebParam(name = "region") Long region, @WebParam(name = "flavor") Long flavor) {
        Map<String, Double> result = new LinkedHashMap();
        
        // Calculate interval duration
        Period intervalDuration = this.getIntervalDuration(intervalType);
        String pattern          = this.getDatePattern(intervalType);
        
        // Calculate total revenue for each interval
        Query q = em.createQuery("SELECT T FROM Transactions T");
        List<Transactions> transactions = q.getResultList();
        
        DateTime intervalEnd = new DateTime(start).plus(intervalDuration);
        while (intervalEnd.getMillis() <= end) {
            double totalRevenue = 0;
            
            Iterator<Transactions> iter = transactions.iterator();
            while (iter.hasNext()) {
                Transactions   t = iter.next();
                long createdDate = t.getCreatedDate();
                
                if (createdDate >= start && createdDate <= intervalEnd.getMillis()) {
                    if (brand == null && region == null && flavor == null) {
                        totalRevenue += t.getTotalAmount();
                        
                    } else {
                        for (TransactionItem item : t.getTransactionItems()) {
                            Product p = item.getProduct();
                        
                            if (brand != null && !p.getCategories().contains(new Category(brand))) continue;

                            if (region != null && !p.getCategories().contains(new Category(region))) continue;

                            if (flavor != null && !p.getCategories().contains(new Category(flavor))) continue;

                            totalRevenue += item.getQuantity() * p.getInfo(createdDate).getPrice();
                        }
                    }
                    
                    iter.remove();
                }
            }
            
            // Prepare for next iteration
            result.put(new DateTime(start).toString(pattern), totalRevenue);
            start       = intervalEnd.getMillis();
            intervalEnd = intervalEnd.plus(intervalDuration);
        }
        
        return new Gson().toJson(result);
    }
}
