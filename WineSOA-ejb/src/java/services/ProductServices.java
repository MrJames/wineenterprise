/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import entities.Category;
import entities.Product;
import entities.ProductInfo;
import entities.Review;
import entities.TransactionItem;
import entities.Transactions;
import entities.Users;
import entityStates.CategoryState;
import entityStates.CategoryStateEntityConverter;
import entityStates.ProductState;
import entityStates.ReviewState;
import entityStates.StateEntityConverter;
import entityStates.TransactionItemState;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.jws.Oneway;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.commons.io.FileUtils;
import utilities.Constants;

/**
 *
 * @author JamesTran
 */
@WebService(serviceName = "ProductServices")
@Stateless()
public class ProductServices {
    @PersistenceContext
    private EntityManager em;
    
    @WebMethod(operationName = "confirmOrder")
    @Oneway
    public void confirmOrder(@WebParam(name = "customerEmail") String customerEmail, @WebParam(name = "paypalID") String paypalID, @WebParam(name = "items") String items, @WebParam(name = "amountPaid") double amountPaid) {
        Users        u = em.find(Users.class, customerEmail);
        if (u == null) return;
        else {
            Query q = em.createQuery("SELECT T FROM Transactions T WHERE T.paypalID = :paypalID");
            q.setParameter("paypalID", paypalID);
            if (!q.getResultList().isEmpty()) return;
            else {
                Transactions t = new Transactions();
                t.createTransaction(amountPaid, u.getShippingAddress1(), u.getShippingAddress2(), u.getShippingCountry(), u.getShippingZipCode(), paypalID, u);
            
                // Process items
                Set<TransactionItemState> transactionItems = new Gson().fromJson(items, new TypeToken<Set<TransactionItemState>>(){}.getType());
                for (TransactionItemState transactionItem : transactionItems) {
                    Product         p    = em.find(Product.class, transactionItem.getId());
                    System.out.println("CONFIRM ORDER: " + p);
                    TransactionItem item = new TransactionItem();
                    item.createTransactionItem(transactionItem.getQuantity(), p);
                    t.addTransactionItem(item);
                }
                
                // Persist transaction
                u.addTransaction(t);
                em.persist(t);
            }
        }
    }
    
    public void uploadFileToS3(String fileName, String s3Folder, byte[] file) {
        try {
            // Prepare the file
            File tmpFile = new File(Constants.SYSTEM_CONTENT_DIRECTORY + fileName);
            FileUtils.writeByteArrayToFile(tmpFile, file);
            
            // Upload to S3
            try {
                AWSCredentials credentials = new BasicAWSCredentials(Constants.SYSTEM_AMAZON_KEY_ID, Constants.SYSTEM_AMAZON_SECRET_KEY);
                TransferManager manager = new TransferManager(credentials);
                PutObjectRequest por = new PutObjectRequest(Constants.SYSTEM_AMAZON_BUCKET_NAME, s3Folder + fileName, tmpFile);
                por.setCannedAcl(CannedAccessControlList.PublicRead);
                Upload upload = manager.upload(por);
                upload.waitForCompletion();

            } catch (AmazonClientException ex) {
                System.out.println("ProductServices - AmazonClientException: " + ex.toString());
            } catch (InterruptedException ex) {
                System.out.println("ProductServices - InterruptedException: " + ex.toString());
            }
            
            // Clean the tmpFile
            tmpFile.delete();
            
        } catch (IOException ex) {
            System.out.println("ProductServices - IOException: " + ex.toString());
        }
    }
    
    @WebMethod(operationName = "createCategory")
    public String createCategory(@WebParam(name = "categoryName") String categoryName, @WebParam(name = "categoryType")  Integer categoryType) {
        Query q = em.createQuery("SELECT C FROM Category C WHERE C.name = :name AND C.type = :type AND C.deleted = 'false'");
        q.setParameter("name", categoryName);
        q.setParameter("type", categoryType);
        if (!q.getResultList().isEmpty()) return null;
        else {
            // Persist the new category
            Category category = new Category();
            category.createCategory(categoryName, categoryType);
            em.persist(category);
            em.flush();
            
            // Return the state
            CategoryState cs = new CategoryState(category.getId(), category.getName(), category.getType(), category.getCreatedDate());
            return new Gson().toJson(cs);
        }
    }
    
    @WebMethod(operationName = "updateCategory")
    public Integer updateCategory(@WebParam(name = "jsonizedCategory") String jsonizedCategory) {
        CategoryState cs = new Gson().fromJson(jsonizedCategory, CategoryState.class);
        Category c = em.find(Category.class, cs.getId());
        
        if (c == null || c.isDeleted()) return Constants.STATUS_NOT_FOUND;
        else {
            // Update the category info
            c.setName(cs.getName());
            c.setType(cs.getType());
            
            return Constants.STATUS_SUCCESSFUL;
        }
    }

    @WebMethod(operationName = "findCategoryByName")
    public String findCategoryByName(@WebParam(name = "categoryName") String categoryName) {
        Query q = em.createQuery("SELECT C FROM Category C WHERE C.name LIKE :name");
        q.setParameter("name", "%" + categoryName + "%");
        
        List<CategoryState> categoryStateList = new ArrayList();
        for (Object o : q.getResultList()) {
            Category c = (Category) o;
            if (!c.isDeleted()) {
                CategoryState cs = new CategoryState(c.getId(), c.getName(), c.getType(), c.getCreatedDate());
                categoryStateList.add(cs);
            }
        }
        
        String categoryListJson = new Gson().toJson(categoryStateList);
        return categoryListJson;
    }

    @WebMethod(operationName = "findCategoryByType")
    public String findCategoryByType(@WebParam(name = "categoryType") Integer categoryType) {
        Query q = em.createQuery("SELECT C FROM Category C WHERE C.type = :type");
        q.setParameter("type", categoryType);

        List<CategoryState> categoryStateList = new ArrayList();
        for (Object o : q.getResultList()) {
            Category c = (Category) o;
            if (!c.isDeleted()) {
                CategoryState cs = new CategoryState(c.getId(), c.getName(), c.getType(), c.getCreatedDate());
                categoryStateList.add(cs);
            }
        }

        String categoryListJson = new Gson().toJson(categoryStateList);
        return categoryListJson;
    }

    @WebMethod(operationName = "findAllCategories")
    public String findAllCategories() {
        Query q = em.createQuery("SELECT C FROM Category C");
            
        List<CategoryState> categoryStateList = new ArrayList();
        for (Object o : q.getResultList()) {
            Category c = (Category) o;
            if (!c.isDeleted()) {
                CategoryState cs = new CategoryState(c.getId(), c.getName(), c.getType(), c.getCreatedDate());
                categoryStateList.add(cs);
            }
        }

        String categoryListJson = new Gson().toJson(categoryStateList);
        return categoryListJson;
    }

    @WebMethod(operationName = "deleteCategory")
    public Integer deleteCategory(@WebParam(name = "id") Long id) {
        Category category = em.find(Category.class, id);
        if (category == null || category.isDeleted()) return Constants.STATUS_NOT_FOUND;
        else {
            // Delete the category
            category.setDeleted(true);
            
            // Delete all products of that category
            for (Product p : category.getProducts()) {
                p.setDeleted(true);
            }
            
            return Constants.STATUS_SUCCESSFUL;
        }
    }

    @WebMethod(operationName = "deleteProduct")
    public Integer deleteProduct(@WebParam(name = "deleteID") Long id) {
        Product p = em.find(Product.class, id);
        if (p == null || p.isDeleted()) return Constants.STATUS_NOT_FOUND;
        else {
            p.setDeleted(true);
            return Constants.STATUS_SUCCESSFUL;
        }
    }

    @WebMethod(operationName = "updateProduct")
    public Integer updateProduct(@WebParam(name = "updateProductString") String productObject) {
        ProductState ps = new Gson().fromJson(productObject, ProductState.class);
        Product       p = em.find(Product.class, ps.getId());
        
        if (p == null || p.isDeleted()) return Constants.STATUS_NOT_FOUND;
        else {
            double currentPrice = p.getLatestPrice();
            if (ps.getPrice() != currentPrice) {
                ProductInfo newInfo = p.updateProductInfo(ps.getPrice());
                em.persist(newInfo);
            }

            if (ps.getName() != null) {
                p.setName(ps.getName());
            }

            if (ps.getDescription() != null) {
                p.setDescription(ps.getDescription());
            }

            if (ps.getPhoto() != null) {
                p.setPhoto(ps.getPhoto());
            }

            if (ps.getQuantity() != null) {
                p.setQuantity(ps.getQuantity());
            }
            
            return Constants.STATUS_SUCCESSFUL;
        }
    }
    
    @WebMethod(operationName = "updateProductCategory")
    public Integer updateProductCategory(@WebParam(name = "updateProductString") String productObject, @WebParam(name = "updateCategoryList") String categories) {
        ProductState ps = new Gson().fromJson(productObject, ProductState.class);
        List<CategoryState> categoryList = new Gson().fromJson(categories, new TypeToken<List<CategoryState>>(){}.getType());
        
        Product p = em.find(Product.class, ps.getId());
        if (p == null || p.isDeleted()) return Constants.STATUS_NOT_FOUND;
        else {
            if (categoryList != null && !categoryList.isEmpty()) {
                p.clearCategories();
                for (CategoryState cs : categoryList) {
                    Category c = em.find(Category.class,cs.getId());
                    if (!c.isDeleted()) p.addCategory(c);
                }
                
                return Constants.STATUS_SUCCESSFUL;
            } else return Constants.STATUS_ILLEGAL_INPUT;
        }
    }

    @WebMethod(operationName = "createProduct")
    public Integer createProduct(@WebParam(name = "jsonizedProduct")  String jsonizedProduct,
                                 @WebParam(name = "jsonizedPhoto")    String jsonizedPhoto,
                                 @WebParam(name = "jsonizedBigPhoto") String jsonizedBigPhoto) {
        try {
            // Persist the product
            ProductState ps = new Gson().fromJson(jsonizedProduct, ProductState.class);
            Product p = new Product();
            p.createProduct(ps.getName(), ps.getDescription(), ps.getCost(), ps.getPrice(), ps.getType(), ps.getQuantity());
       
            for (CategoryState cs : ps.getCategories()) {
                Category c = em.find(Category.class,cs.getId());
                if (!c.isDeleted()) {
                    p.addCategory(c);
                    c.addProduct(p);
                }
            }
            
            em.persist(p);
            em.flush();
            
            // Upload photo to S3
            byte[] photo = new Gson().fromJson(jsonizedPhoto, byte[].class);
            String photoName = p.getId() + ".png";
            this.uploadFileToS3(photoName, Constants.SYSTEM_AMAZON_S3_PHOTO_FOLDER, photo);
            
            byte[] bigPhoto = new Gson().fromJson(jsonizedBigPhoto, byte[].class);
            String bigPhotoName = p.getId() + "big.png";
            this.uploadFileToS3(bigPhotoName, Constants.SYSTEM_AMAZON_S3_PHOTO_FOLDER, bigPhoto);
            
            p.setPhoto(photoName);
            
            return Constants.STATUS_SUCCESSFUL;
        } catch (Exception ex) {
            System.out.println("CREATE PRODUCT: " + ex.toString());
            return Constants.STATUS_UNEXPECTED_ERROR;
        }
    }
    
    @WebMethod(operationName = "getProductCategories")
    public String getProductCategories(@WebParam(name = "productID") Long id) {
        Product p = em.find(Product.class, id);
        if (p == null) return null;
        else {
            List<CategoryState> categoryStateList = new ArrayList();
            for (Category category : p.getCategories()) {
                if (!category.isDeleted()) {
                    categoryStateList.add(CategoryStateEntityConverter.converToState(category));
                }
            }

            String categoryListJson = new Gson().toJson(categoryStateList);
            return categoryListJson;
        }
    }
    
    @WebMethod(operationName = "getAllProducts")
    public String getAllProducts() {
        Query q = em.createQuery("SELECT P FROM Product P");
        List<ProductState> productList = new ArrayList();

        for (Object o : q.getResultList()) {
            Product p = (Product) o;
            if (!p.isDeleted()) {
                double       price = p.getLatestPrice();
                ProductState ps    = new ProductState(p.getId(), p.getName(), p.getDescription(), p.getPhoto(), p.getCost(), price, p.getType(), p.getQuantity(), p.getTotalRating(), p.getNumberOfReviewers(), p.getCreatedDate());
                
                for (Category c : p.getCategories()) {
                    CategoryState cs = new CategoryState(c.getId(), c.getName(), c.getType());
                    ps.addCategory(cs);
                }
                
                productList.add(ps);
            }
        }

        String productListJson = new Gson().toJson(productList);
        return productListJson;
    }

    @WebMethod(operationName = "searchProduct")
    public String searchProduct(@WebParam(name = "productName") String productName) {
        System.out.println("SEARCH: " + productName);
        Query q = em.createQuery("SELECT P FROM Product P WHERE P.name LIKE :name");
        q.setParameter("name", "%" + productName + "%");
        List<Object> objList = q.getResultList();
        System.out.println("SEARCH: " + objList.size());

        List<ProductState> productStateList = new ArrayList<ProductState>();
        for (Object o : objList) {
            Product p = (Product) o;
            if (!p.isDeleted()) productStateList.add(StateEntityConverter.createProductState(p));
        }

        String productListJson = new Gson().toJson(productStateList);
        return productListJson;
    }
    
    @WebMethod(operationName = "RateProduct")
    public Integer rateProduct(@WebParam(name = "transactionId") Long transactionId, @WebParam(name = "productId") Long productId, @WebParam(name = "userEmail") String userEmail, @WebParam(name = "rating") Integer rating) {
        Users u = em.find(Users.class, userEmail);
        if (u == null || u.isDeleted()) return Constants.STATUS_NOT_FOUND;
        else if (u.isBlocked() || u.isDeleted()) return Constants.STATUS_ILLEGAL_STATE;
        else {
            Transactions t = em.find(Transactions.class, transactionId);
            Product      p = em.find(Product.class, productId);
            if (t == null || p == null) return Constants.STATUS_NOT_FOUND;
            else {
                try {
                    Query q = em.createQuery("SELECT R FROM Product P JOIN P.reviews R WHERE P.id = :productID AND R.reviewer = :reviewer AND R.transaction.id = :transactionID");
                    q.setParameter("productID", productId);
                    q.setParameter("reviewer", userEmail);
                    q.setParameter("transactionID", transactionId);

                    // Update old review with new rating
                    Review r = (Review) q.getSingleResult();
                    int oldRating  = r.getRating();
                    int difference = rating - oldRating;
                    p.updateRatingPoint(difference);
                    r.setRating(rating);
                    
                    
                } catch (NoResultException ex) {
                    // Create new rating
                    Review r = new Review();
                    r.createReview(userEmail, rating, t);
                    p.addReview(r);
                    p.addRatingPoint(rating);
                    em.persist(r);
                }
                
                return Constants.STATUS_SUCCESSFUL;
            }
        }
    }
    
    @WebMethod(operationName = "AddReview")
    public Integer addReview(@WebParam(name = "productId") Long productId, @WebParam(name = "userEmail") String userEmail, @WebParam(name = "comment") String comment, @WebParam(name = "rating") Integer rating) {
        // check whether user has bought the product

        //TransactionsState transactInfo = new Gson().fromJson(productObject, TransactionsState.class);
        Users u = em.find(Users.class, userEmail);
        if (u == null || u.isDeleted()) return Constants.STATUS_NOT_FOUND;
        else if (u.isBlocked()) return Constants.STATUS_ILLEGAL_STATE;
        else if (u.isIsAdmin()) {
            Product p = em.find(Product.class, productId);
            if (p == null) return Constants.STATUS_NOT_FOUND;
            else {
                Review r = new Review();
           
                r.setReviewer(userEmail);
//                r.setComment(comment);
                r.setRating(rating);
                p.getReviews().add(r);
                em.persist(r);
                
                return Constants.STATUS_SUCCESSFUL;
            }
            
        } else {
            List<Transactions> transactions = u.getTransactions();

            for (Transactions t : transactions) {
                Set<TransactionItem> items = t.getTransactionItems();
                for (TransactionItem item : items) {
                    Product p = item.getProduct();
                    if (p.getId() == productId) {
                        Review r = new Review();
                        r.setReviewer(userEmail);
//                        r.setComment(comment);
                        r.setRating(rating);
                        r.setTransaction(t);
                        p.getReviews().add(r);
                        em.persist(r);
                        
                        return Constants.STATUS_SUCCESSFUL;
                    }
                }

            }
            
            return Constants.STATUS_FAILED;
        }
    }

    @WebMethod(operationName = "getReviews")
    public String getReviews(@WebParam(name = "productId") Long productId, @WebParam(name = "firstPos") Integer firstPos) {
        Query q = em.createQuery("SELECT R FROM Product P JOIN P.reviews R WHERE P.id = :id ORDER BY R.createdDate DESC");
        q.setParameter("id", productId);
        q.setFirstResult(firstPos);
        q.setMaxResults(3);
        
        Iterator it = q.getResultList().iterator();
        List<ReviewState> reviewList = new <ReviewState> ArrayList();
        
        while (it.hasNext()) {
            Review review = (Review) it.next();
            ReviewState r = new ReviewState();
            r.setId(review.getId());
            r.setReviewer(review.getReviewer());
//            r.setComment(review.getComment());
            r.setRating(review.getRating());
            r.setCreatedDate(review.getCreatedDate());
            reviewList.add(r);
        }
        
        String reviewListJson = new Gson().toJson(reviewList);
        return reviewListJson;
    }

    @WebMethod(operationName = "ShowAvgRating")
    public Double showAvgRating(@WebParam(name = "productId") Long productId) {
        Product p = em.find(Product.class, productId);
        if (p == null || p.isDeleted()) return null;
        else {
            Double avgRating = Double.valueOf(p.getTotalRating()) / p.getNumberOfReviewers();
            return avgRating;
        }
    }
    
    @WebMethod(operationName = "getProductDetails")
    public String getProductDetails(@WebParam(name = "productID") Long productID) {
        Product p = em.find(Product.class, productID);
        if (p == null || p.isDeleted()) return null;
        else {
            double       price = p.getLatestPrice();
            ProductState ps    = new ProductState(p.getId(), p.getName(), p.getDescription(), p.getPhoto(), p.getCost(), price, p.getType(), p.getQuantity(), p.getTotalRating(), p.getNumberOfReviewers(), p.getCreatedDate());

            for (Category c : p.getCategories()) {
                CategoryState cs = new CategoryState(c.getId(), c.getName(), c.getType());
                ps.addCategory(cs);
            }
            
            return new Gson().toJson(ps);
        }
    }
}
