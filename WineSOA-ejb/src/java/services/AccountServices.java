/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.google.gson.Gson;
import entities.InactivatedUser;
import entities.Product;
import entities.TransactionItem;
import entities.Transactions;
import entities.Users;
import entityStates.ProductState;
import entityStates.TransactionItemState;
import entityStates.TransactionsState;
import entityStates.UserState;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;
import utilities.Constants;

/**
 *
 * @author JamesTran
 */
@WebService(serviceName = "AccountServices")
@Stateless()
public class AccountServices {
    //Resources
    @Resource(mappedName = "jms/IS4227QueueConnectionFactory")
    private ConnectionFactory queueConnectionFactory;
    @Resource(mappedName = "jms/IS4227OutgoingEmailQueue")
    private Queue emailQueue;
    
    //Bean's variables
    @PersistenceContext()
    private EntityManager em;
    
    @WebMethod(operationName = "recoverPassword")
    public Integer recoverPassword(@WebParam(name = "email") String email) {
        Users u = em.find(Users.class, email);
            
        if (u == null || u.isDeleted()) return Constants.STATUS_NOT_FOUND;
        else {
            // Hash new password
            String password = RandomStringUtils.randomAlphanumeric(10);
            u.setPassword(DigestUtils.sha256Hex(password + u.getSalt()));
            
            // Send the new password
            try { 
                sendEmail(Constants.EMAIL_FORGOT, email, null, password); 
                
            } catch (JMSException ex) { 
                System.out.println("JMSException: " + ex.toString()); 
                
            }
            
            return Constants.STATUS_SUCCESSFUL;
        }
    }
    
    @WebMethod(operationName = "updateContactInfo")
    public Integer updateContactInfo(@WebParam(name = "contactInfo") String jsonizedInfo) {
        UserState contactInfo = new Gson().fromJson(jsonizedInfo, UserState.class);
        Users u = em.find(Users.class, contactInfo.getEmail());
            
        if (u == null || u.isDeleted()) return Constants.STATUS_NOT_FOUND;
        else {
            // Update contact info
            u.setCountryCode(contactInfo.getCountryCode());
            u.setPhone(contactInfo.getPhone());
            u.setBillingAddress1(contactInfo.getBillingAddress1());
            u.setBillingAddress2(contactInfo.getBillingAddress2());
            u.setBillingCountry(contactInfo.getBillingCountry());
            u.setBillingZipCode(contactInfo.getBillingZipCode());
            u.setShippingAddress1(contactInfo.getShippingAddress1());
            u.setShippingAddress2(contactInfo.getShippingAddress2());
            u.setShippingCountry(contactInfo.getShippingCountry());
            u.setShippingZipCode(contactInfo.getShippingZipCode());
        
            return Constants.STATUS_SUCCESSFUL;
        }
    }
    
    @WebMethod(operationName = "getContactInfo")
    public String getContactInfo(@WebParam(name = "userEmail") String userEmail) {
        Users u = em.find(Users.class, userEmail);
            
        if (u == null || u.isDeleted()) return null;
        else {
            UserState us = new UserState(u.getEmail(), u.getCountryCode(), u.getPhone(), u.getBillingAddress1(), u.getBillingAddress2(), u.getBillingCountry(), u.getBillingZipCode(), u.getShippingAddress1(), u.getShippingAddress2(), u.getShippingCountry(), u.getShippingZipCode());
            String jsonizedObject = new Gson().toJson(us);
            return jsonizedObject;
        }
    }
    
    @WebMethod(operationName = "getUserInfo")
    public String getUserInfo(@WebParam(name = "userEmail") String userEmail) {
        Users u = em.find(Users.class, userEmail);
            
        if (u == null || u.isDeleted()) return null;
        else {
            UserState us = new UserState(u.getEmail(), u.getFirstName(), u.getLastName(), u.getGender(), u.isActivated(), u.isDeleted(), u.isBlocked(), u.isIsAdmin());
            String jsonizedObject = new Gson().toJson(us);
            return jsonizedObject;
        }
    }
    
    @WebMethod(operationName = "toggleUserBlockStatus")
    public Integer toggleUserBlockStatus(@WebParam(name = "adminEmail") String adminEmail, @WebParam(name = "adminPassword") String adminPassword, @WebParam(name = "userEmail") String userEmail) {
        Users admin = em.find(Users.class, adminEmail);
        
        if (admin == null) return Constants.STATUS_FAILED;
        else if (!admin.getPassword().equals(DigestUtils.sha256Hex(adminPassword + admin.getSalt()))) return Constants.STATUS_WRONG_PASSWORD;
        else {
            Users u = em.find(Users.class, userEmail);
            
            if (u == null) return Constants.STATUS_NOT_FOUND;
            else {
                Boolean blockStatus = u.isBlocked();
                u.setBlocked(!blockStatus);
                return Constants.STATUS_SUCCESSFUL;
            }
        }
    }
    
    @WebMethod(operationName = "deleteUser")
    public Integer deleteUser(@WebParam(name = "adminEmail") String adminEmail, @WebParam(name = "adminPassword") String adminPassword, @WebParam(name = "userEmail") String userEmail) {
        Users admin = em.find(Users.class, adminEmail);
        
        if (admin == null) return Constants.STATUS_FAILED;
        else if (!admin.getPassword().equals(DigestUtils.sha256Hex(adminPassword + admin.getSalt()))) return Constants.STATUS_WRONG_PASSWORD;
        else {
            Users u = em.find(Users.class, userEmail);
            
            if (u == null) return Constants.STATUS_NOT_FOUND;
            else {
                u.setDeleted(true);
                return Constants.STATUS_SUCCESSFUL;
            }
        }
    }
    
    @WebMethod(operationName = "cancelAccount")
    public Integer cancelAccount(@WebParam(name = "email") String email, @WebParam(name = "password") String password) {
        Users u = em.find(Users.class, email);
        
        if (u == null) return Constants.STATUS_NOT_FOUND;
        else if (!u.getPassword().equals(DigestUtils.sha256Hex(password + u.getSalt()))) return Constants.STATUS_WRONG_PASSWORD;
        else {
            u.setDeleted(true);
            return Constants.STATUS_SUCCESSFUL;
        }
    }
    
    @WebMethod(operationName = "changePassword")
    public Integer changePassword(@WebParam(name = "email") String email, @WebParam(name = "currentPassword") String currentPassword, @WebParam(name = "newPassword") String newPassword) {
        Users u = em.find(Users.class, email);
        
        if (u == null) return Constants.STATUS_NOT_FOUND;
        else if (!u.getPassword().equals(DigestUtils.sha256Hex(currentPassword + u.getSalt()))) return Constants.STATUS_WRONG_PASSWORD;
        else {
            u.setPassword(DigestUtils.sha256Hex(newPassword + u.getSalt()));
            return Constants.STATUS_SUCCESSFUL;
        }
    }
    
    @WebMethod(operationName = "validatePassword")
    public Boolean validatePassword(@WebParam(name = "email") String email, @WebParam(name = "password") String password) {
        Users u = em.find(Users.class, email);
        if (u == null || !u.getPassword().equals(DigestUtils.sha256Hex(password + u.getSalt()))) return false;
        else return true;
    }
    
    @WebMethod(operationName = "login_2")
    @RequestWrapper(className = "services.login_2")
    @ResponseWrapper(className = "services.login_2Response")
    public String login(@WebParam(name = "email") String email) {
        Users u = em.find(Users.class, email);
        if (u == null) return null;
        else {
            UserState us = null;
            
            if (u.isBlocked()) {
                us = new UserState();
                us.setActivated(true);
                us.setBlocked(true);
                
            } else if (u.isDeleted()) {
                us = new UserState();
                us.setActivated(true);
                us.setBlocked(false);
                us.setDeleted(true);
                
            } else {
                us = new UserState(u.getEmail(), u.getFirstName(), u.getLastName(), u.getGender(), u.isIsAdmin());
                us.setActivated(true);
                us.setBlocked(false);
                us.setDeleted(false);
                
            }
            
            String jsonizedObject = new Gson().toJson(us);
            return jsonizedObject;
        }
    }
    
    @WebMethod(operationName = "login_1")
    @RequestWrapper(className = "services.login_1")
    @ResponseWrapper(className = "services.login_1Response")
    public String login(@WebParam(name = "email") String email, @WebParam(name = "password") String password) {
        Users u = em.find(Users.class, email);
        if (u == null || !u.getPassword().equals(DigestUtils.sha256Hex(password + u.getSalt()))) return null;
        else {
            UserState us = null;
            
            if (!u.isActivated()) {
                us = new UserState();
                us.setActivated(false);
                
            } else if (u.isBlocked()) {
                us = new UserState();
                us.setActivated(true);
                us.setBlocked(true);
                
            } else if (u.isDeleted()) {
                us = new UserState();
                us.setActivated(true);
                us.setBlocked(false);
                us.setDeleted(true);
                
            } else {
                us = new UserState(u.getEmail(), u.getFirstName(), u.getLastName(), u.getGender(), u.isIsAdmin());
                us.setActivated(true);
                us.setBlocked(false);
                us.setDeleted(false);
                
            }
            
            String jsonizedObject = new Gson().toJson(us);
            return jsonizedObject;
        }
    }
   
    @WebMethod(operationName = "checkEmailExistence")
    public Integer checkEmailExistence(@WebParam(name = "email") String email) {
        if (em.find(Users.class, email) == null) return Constants.STATUS_SUCCESSFUL;
        else return Constants.STATUS_NOT_UNIQUE;
    }
    
    @WebMethod(operationName = "activateMember")
    public Integer activateMember(@WebParam(name = "id") String id) {
        InactivatedUser iu = em.find(InactivatedUser.class, id);
        if (iu == null) return Constants.STATUS_NOT_FOUND;
        else {
            Users u = iu.getUser();
            u.setActivated(true);
            em.remove(iu);
            em.flush();
            
            try { 
                sendEmail(Constants.EMAIL_WELCOME, u.getEmail(), null); 
            
            } catch (JMSException ex) { 
                System.out.println("JMSException: " + ex.toString()); 
                
            }
            
            // Subscribe to Amazon SNS
            AWSCredentials credentials = new BasicAWSCredentials(Constants.SYSTEM_AMAZON_KEY_ID, Constants.SYSTEM_AMAZON_SECRET_KEY);		
            AmazonSNSClient snsClient = new AmazonSNSClient(credentials);
            snsClient.setEndpoint("sns.ap-southeast-1.amazonaws.com");

            SubscribeRequest sr = new SubscribeRequest(Constants.SYSTEM_AMAZON_SNS_TOPIC_ARN, "email", u.getEmail());
            snsClient.subscribe(sr);
            
            return Constants.STATUS_SUCCESSFUL;
        }
    }
    
    @WebMethod(operationName = "registerAdmin_1")
    @RequestWrapper(className = "services.registerAdmin_1")
    @ResponseWrapper(className = "services.registerAdmin_1Response")
    public Integer registerAdmin(@WebParam(name = "email") String email, @WebParam(name = "gender") String gender, @WebParam(name = "firstName") String firstName, @WebParam(name = "lastName") String lastName, @WebParam(name = "activationLink") String activationLink) {
        //Check if the database contains user with same IC or email
        Users u = em.find(Users.class, email);
        
        if (u != null) return Constants.STATUS_NOT_UNIQUE;
        else {
            // Create the user
            u = new Users();
            u.createAdmin(email, firstName, lastName, gender);
            
            // Hash user's password
            String password = RandomStringUtils.randomAlphanumeric(10);
            String salt     = DigestUtils.sha256Hex(RandomStringUtils.randomAlphanumeric(10));
            u.setSalt(salt);
            u.setPassword(DigestUtils.sha256Hex(password + salt));
            
            // Get a uniqueID
            String uniqueID;
            do {
                uniqueID = RandomStringUtils.randomAlphanumeric(10);
            } while (em.find(InactivatedUser.class, uniqueID) != null);
            
            // Persist the new user
            InactivatedUser iu = new InactivatedUser(uniqueID, u);
            em.persist(iu);
            em.flush();
            
            // Send activation email to the new user
            String link = activationLink + "?id=" + uniqueID;
            try { 
                sendEmail(Constants.EMAIL_STAFF_ACTIVATION, email, link, email, password); 
                
            } catch (JMSException ex) { 
                System.out.println("JMSException: " + ex.toString()); 
                
            }
            
            return Constants.STATUS_SUCCESSFUL;
        }
    }
    
    @WebMethod(operationName = "activateAdmin")
    public String activateAdmin(@WebParam(name = "email") String email, @WebParam(name = "firstName") String firstName, @WebParam(name = "lastName") String lastName, @WebParam(name = "gender") String gender) {
        Users u = em.find(Users.class, email);
        if (u == null || !u.isIsAdmin()) return null;
        else {
            u.updateAdmin(firstName, lastName, gender);

            try { 
                sendEmail(Constants.EMAIL_WELCOME, u.getEmail(), null); 

            } catch (JMSException ex) { 
                System.out.println("JMSException: " + ex.toString()); 

            }

            // Return user info
            UserState us = new UserState(u.getEmail(), u.getFirstName(), u.getLastName(), u.getGender(), u.isIsAdmin());
            String jsonizedObject = new Gson().toJson(us);
            return jsonizedObject;
        }
    }
    
    @WebMethod(operationName = "registerAdmin_2")
    @RequestWrapper(className = "services.registerAdmin_2")
    @ResponseWrapper(className = "services.registerAdmin_2Response")
    public Integer registerAdmin(@WebParam(name = "email") String email, @WebParam(name = "activationLink") String activationLink) {
        //Check if the database contains user with same IC or email
        Users u = em.find(Users.class, email);
        
        if (u != null) return Constants.STATUS_NOT_UNIQUE;
        else {
            // Create the user
            u = new Users();
            u.createAdmin(email);
            em.persist(u);
            
            // Send activation email to the new user
            try { 
                sendEmail(Constants.EMAIL_STAFF_ACTIVATION, email, activationLink); 
                
            } catch (JMSException ex) { 
                System.out.println("JMSException: " + ex.toString()); 
                
            }
            
            return Constants.STATUS_SUCCESSFUL;
        }
    }
    
    @WebMethod(operationName = "registerMember_2")
    @RequestWrapper(className = "services.registerMember_2")
    @ResponseWrapper(className = "services.registerMember_2Response")
    public String registerMember(@WebParam(name = "email") String email, @WebParam(name = "firstName") String firstName, @WebParam(name = "lastName") String lastName, @WebParam(name = "gender") String gender) {
        // Create the user
        Users u = new Users();
        u.createMember(email, firstName, lastName, gender);
        
        // Hash user's password
        String salt = DigestUtils.sha256Hex(RandomStringUtils.randomAlphanumeric(10));
        u.setSalt(salt);
        u.setPassword(DigestUtils.sha256Hex("wineexpress" + salt));
        
        // Persist the new user
        em.persist(u);
        em.flush();
        
        // Subscribe to Amazon SNS
        AWSCredentials credentials = new BasicAWSCredentials(Constants.SYSTEM_AMAZON_KEY_ID, Constants.SYSTEM_AMAZON_SECRET_KEY);		
        AmazonSNSClient snsClient = new AmazonSNSClient(credentials);
        snsClient.setEndpoint("sns.ap-southeast-1.amazonaws.com");

        SubscribeRequest sr = new SubscribeRequest(Constants.SYSTEM_AMAZON_SNS_TOPIC_ARN, "email", email);
        snsClient.subscribe(sr);
        
        // Return user info
        UserState us = new UserState(u.getEmail(), u.getFirstName(), u.getLastName(), u.getGender(), u.isIsAdmin());
        String jsonizedObject = new Gson().toJson(us);
        return jsonizedObject;
    }
    
    @WebMethod(operationName = "registerMember_1")
    @RequestWrapper(className = "services.registerMember_1")
    @ResponseWrapper(className = "services.registerMember_1Response")
    public Integer registerMember(@WebParam(name = "email") String email, @WebParam(name = "password") String password, @WebParam(name = "gender") String gender, @WebParam(name = "firstName") String firstName, @WebParam(name = "lastName") String lastName, @WebParam(name = "countryCode") String countryCode, @WebParam(name = "phone") String phone, @WebParam(name = "activationLink") String activationLink) {
        //Check if the database contains user with same IC or email
        Users u = em.find(Users.class, email);
        
        if (u != null) return Constants.STATUS_NOT_UNIQUE;
        else {
            // Create the user
            u = new Users();
            u.createMember(email, firstName, lastName, gender, countryCode, phone);
            
            // Hash user's password
            String salt = DigestUtils.sha256Hex(RandomStringUtils.randomAlphanumeric(10));
            u.setSalt(salt);
            u.setPassword(DigestUtils.sha256Hex(password + salt));
            
            // Get a uniqueID
            String uniqueID;
            do {
                uniqueID = RandomStringUtils.randomAlphanumeric(10);
            } while (em.find(InactivatedUser.class, uniqueID) != null);
            
            // Persist the new user
            InactivatedUser iu = new InactivatedUser(uniqueID, u);
            em.persist(iu);
            em.flush();
            
            // Send activation email to the new user
            String link = activationLink + "?id=" + uniqueID;
            try { 
                sendEmail(Constants.EMAIL_MEMBER_ACTIVATION, email, link); 
                
            } catch (JMSException ex) { 
                System.out.println("JMSException: " + ex.toString()); 
                
            }
            
            return Constants.STATUS_SUCCESSFUL;
        }
    }
    
    @WebMethod(operationName = "submitEnquiry")
    public Integer submitEnquiry(@WebParam(name = "name") String name, @WebParam(name = "email") String email, @WebParam(name = "subject") String subject, @WebParam(name = "enquiry") String enquiry) {
        try {
            sendEmail(Constants.EMAIL_ENQUIRY, Constants.SYSTEM_MAILBOX_ACCOUNT, null, name, email, subject, enquiry);
            sendEmail(Constants.EMAIL_ENQUIRY_AUTO_REPLY, email, null, subject, enquiry);
            return Constants.STATUS_SUCCESSFUL;
        } catch (JMSException ex) {
            return Constants.STATUS_UNEXPECTED_ERROR;
        }
    }
    
    private void sendEmail(int msgType, String recipient, String link, String... args) throws JMSException {
        Connection      queueConnection = queueConnectionFactory.createConnection();
        Session         queueSession    = queueConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        MessageProducer queueProducer   = queueSession.createProducer(emailQueue);
        MapMessage      msg             = queueSession.createMapMessage();
        int             i               = 1;
        //Plug in all data available and send the message
        msg.setInt("msgType", msgType);
        msg.setString("recipient", recipient);
        if (link != null) msg.setString("link", link);
        for (String str : args) {
            msg.setString("str"+i, str);
            i++;
        }
        queueProducer.send(msg);
        queueConnection.close();
    }

    @WebMethod(operationName = "getNumberOfTransactions")
    public Integer getNumberOfTransactions(@WebParam(name = "email") String email) {
        Users u = em.find(Users.class, email);
        if (u == null) return 0;
        else return u.getTransactions().size();
    }
    
    @WebMethod(operationName = "loadTransactionHistory")
    public String loadTransactionHistory(@WebParam(name = "email") String email, @WebParam(name = "firstPos") Integer firstPos, @WebParam(name = "pageSize") Integer pageSize) {
        List<TransactionsState> result = new LinkedList();
        
        Query q = em.createQuery("SELECT T FROM Users U JOIN U.transactions T WHERE U.email = :email ORDER BY T.createdDate DESC");
        q.setParameter("email", email);
        q.setFirstResult(firstPos);
        q.setMaxResults(pageSize);
        
        Iterator<Transactions> iter = q.getResultList().iterator();
        while (iter.hasNext()) {
            Transactions       t = iter.next();
            TransactionsState ts = new TransactionsState(t.getId(), t.getCreatedDate(), t.getTotalAmount(), t.getPaypalID());
            
            for (TransactionItem item : t.getTransactionItems()) {
                Product       p = item.getProduct();
                ProductState ps = new ProductState(p.getId(), p.getName(), p.getInfo(t.getCreatedDate()).getPrice());
                TransactionItemState tis = new TransactionItemState(item.getId(), item.getQuantity(), ps);
                ts.addTransactionItem(tis);
            }
            
            result.add(ts);
        }
        
        return new Gson().toJson(result);
    }
}
