/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author JamesTran
 */
@Entity
public class Users implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private String email;
    private String password;
    private String salt;
    private String firstName;
    private String lastName;
    private String gender;
    private String creditCard;
    private String countryCode;
    private String phone;
    private String billingAddress1;
    private String billingAddress2;
    private String billingCountry;
    private String billingZipCode;
    private String shippingAddress1;
    private String shippingAddress2;
    private String shippingCountry;
    private String shippingZipCode;
    private Long   createdDate;
    private boolean activated;
    private boolean deleted;
    private boolean blocked;
    private boolean isAdmin;
    @OneToMany
    private List<Transactions> transactions;

    public Users() {
        this.createdDate  = System.currentTimeMillis();
        this.activated    = false;
        this.deleted      = false;
        this.blocked      = false;
        this.transactions = new LinkedList();
    }
    
    public void addTransaction(Transactions transaction) {
        this.transactions.add(transaction);
    }

    public void createMember(String email, String firstName, String lastName, String gender, String countryCode, String phone) {
        this.email       = email;
        this.firstName   = firstName;
        this.lastName    = lastName;
        this.gender      = gender;
        this.countryCode = countryCode;
        this.phone       = phone;
        this.isAdmin     = false;
    }
    
    public void createMember(String email, String firstName, String lastName, String gender) {
        this.email       = email;
        this.firstName   = firstName;
        this.lastName    = lastName;
        this.gender      = gender;
        this.isAdmin     = false;
    }
    
    public void createAdmin(String email, String firstName, String lastName, String gender) {
        this.email       = email;
        this.firstName   = firstName;
        this.lastName    = lastName;
        this.gender      = gender;
        this.isAdmin     = true;
    }
    
    public void createAdmin(String email) {
        this.email       = email;
        this.isAdmin     = true;
    }
    
    public void updateAdmin(String firstName, String lastName, String gender) {
        this.firstName   = firstName;
        this.lastName    = lastName;
        this.gender      = gender;
    }
    
    // Getters and Setters
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBillingCountry() {
        return billingCountry;
    }

    public void setBillingCountry(String billingCountry) {
        this.billingCountry = billingCountry;
    }

    public String getShippingCountry() {
        return shippingCountry;
    }

    public void setShippingCountry(String shippingCountry) {
        this.shippingCountry = shippingCountry;
    }

    public List<Transactions> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transactions> transactions) {
        this.transactions = transactions;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBillingAddress1() {
        return billingAddress1;
    }

    public void setBillingAddress1(String billingAddress1) {
        this.billingAddress1 = billingAddress1;
    }

    public String getBillingAddress2() {
        return billingAddress2;
    }

    public void setBillingAddress2(String billingAddress2) {
        this.billingAddress2 = billingAddress2;
    }

    public String getShippingAddress1() {
        return shippingAddress1;
    }

    public void setShippingAddress1(String shippingAddress1) {
        this.shippingAddress1 = shippingAddress1;
    }

    public String getShippingAddress2() {
        return shippingAddress2;
    }

    public void setShippingAddress2(String shippingAddress2) {
        this.shippingAddress2 = shippingAddress2;
    }

    public String getBillingZipCode() {
        return billingZipCode;
    }

    public void setBillingZipCode(String billingZipCode) {
        this.billingZipCode = billingZipCode;
    }

    public String getShippingZipCode() {
        return shippingZipCode;
    }

    public void setShippingZipCode(String shippingZipCode) {
        this.shippingZipCode = shippingZipCode;
    }
    
    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public boolean isIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (email != null ? email.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Users)) {
            return false;
        }
        Users other = (Users) object;
        if ((this.email == null && other.email != null) || (this.email != null && !this.email.equals(other.email))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.User[ id=" + email + " ]";
    }
}
