/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author JamesTran
 */
@Entity
public class Review implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long    id;
    private String  reviewer;
    private Integer rating;
    private Long    createdDate;
    @ManyToOne
    private Transactions transaction;

    public Review() {
        this.createdDate = System.currentTimeMillis();
    }

    public void createReview(String reviewer, Integer rating, Transactions transaction) {
        this.reviewer    = reviewer;
        this.rating      = rating;
        this.transaction = transaction;
    }
    
    // Getters and Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReviewer() {
        return reviewer;
    }

    public void setReviewer(String reviewer) {
        this.reviewer = reviewer;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public Transactions getTransaction() {
        return transaction;
    }

    public void setTransaction(Transactions transaction) {
        this.transaction = transaction;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Review other = (Review) obj;
        if ((this.reviewer == null) ? (other.reviewer != null) : !this.reviewer.equals(other.reviewer)) {
            return false;
        }
        if (this.transaction != other.transaction && (this.transaction == null || !this.transaction.equals(other.transaction))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Review[ id=" + id + " ]";
    }
    
}
