/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *
 * @author JamesTran
 */
@Entity
public class InactivatedUser implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private String    id;
    @OneToOne(cascade={CascadeType.PERSIST})
    private Users user;
    private long  createdDate;
    
    public InactivatedUser() {}

    public InactivatedUser(String id, Users user) {
        this.id          = id;
        this.user        = user;
        this.createdDate = System.currentTimeMillis();
    }
    
    // Getters and Setters
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InactivatedUser)) {
            return false;
        }
        InactivatedUser other = (InactivatedUser) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.InactivatedUser[ id=" + id + " ]";
    }
    
}
