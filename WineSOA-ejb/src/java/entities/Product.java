/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

/**
 *
 * @author JamesTran
 */
@Entity
public class Product implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long    id;
    private String  name;
    private String  photo;
    private Double  cost;
    private Integer type;
    private Integer quantity;
    private Integer totalRating;
    private Integer numberOfReviewers;
    private Long    createdDate;
    private boolean deleted;
    @Column(columnDefinition = "TEXT")
    private String  description;
    @ManyToMany
    private Set<Category>     categories;
    @OneToMany(cascade={CascadeType.PERSIST})
    private List<ProductInfo> infos;
    @OneToMany
    private List<Review>      reviews;

    public Product() {
        this.createdDate       = System.currentTimeMillis();
        this.quantity          = 0;
        this.totalRating       = 0;
        this.numberOfReviewers = 0;
        this.deleted    = false;
        this.categories = new HashSet();
        this.infos      = new LinkedList();
        this.reviews    = new LinkedList();
    }

    public void createProduct(String name, String description, Double cost, Double price, Integer type, Integer quantity) {
        this.name        = name;
        this.description = description;
        this.cost        = cost;
        this.type        = type;
        this.quantity    = quantity;
        
        ProductInfo info = new ProductInfo();
        info.createProductInfo(price, createdDate);
        infos.add(info);
    }
    
    public double getLatestPrice() {
        int size = infos.size();
        return infos.get(size - 1).getPrice();
    }
    
    public ProductInfo getInfo(Long time) {
        for (ProductInfo info : infos) {
            if (info.getStartDate() <= time) {
                if (info.getEndDate() > time || info.getEndDate() == 0) return info;
            }
        }
        
        return null;
    }
    
    public ProductInfo updateProductInfo(Double price) {
        ProductInfo newInfo     = new ProductInfo();
        ProductInfo currentInfo = infos.get(infos.size() - 1);
        Long        currentTime = System.currentTimeMillis();
        currentInfo.setEndDate(currentTime);
        newInfo.createProductInfo(price, currentTime + 1);
        infos.add(newInfo);
        
        return newInfo;
    }
    
    public void addCategory(Category c) {
        this.categories.add(c);
    }
    
    public void addReview(Review r) {
        this.reviews.add(r);
    }
    
    public void removeReview(Review r) {
        this.reviews.remove(r);
    }
    
    public void updateRatingPoint(int difference) {
        this.totalRating += difference;
    }
    
    public void addRatingPoint(int rating) {
        this.totalRating += rating;
        this.numberOfReviewers++;
    }
    
    public void clearCategories() {
        this.categories.clear();
    }
    
    // Getters and Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getName() {
        return name;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getTotalRating() {
        return totalRating;
    }

    public void setTotalRating(Integer totalRating) {
        this.totalRating = totalRating;
    }

    public Integer getNumberOfReviewers() {
        return numberOfReviewers;
    }

    public void setNumberOfReviewers(Integer numberOfReviewers) {
        this.numberOfReviewers = numberOfReviewers;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    public List<ProductInfo> getInfos() {
        return infos;
    }

    public void setInfos(List<ProductInfo> infos) {
        this.infos = infos;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Product)) {
            return false;
        }
        Product other = (Product) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Product[ id=" + id + " ]";
    }
    
}
