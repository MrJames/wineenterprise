/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.*;
import java.lang.reflect.Type;
import entityStates.CategoryState;

/**
 *
 * @author user
 */
public class CategoryDeserializer implements JsonDeserializer<CategoryState[]>
{

    @Override
    public CategoryState[] deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException 
    
    {
        if (json instanceof JsonArray)
    {
      return new Gson().fromJson(json, CategoryState[].class);
    }
    CategoryState child = context.deserialize(json, CategoryState.class);
    return new CategoryState[] { child };
    }

   
                }