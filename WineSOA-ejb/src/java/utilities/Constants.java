/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 *
 * @author JamesTran
 */
public class Constants {
    // Category type
    public static final int CATEGORY_TYPE_REGION   = 0;
    public static final int CATEGORY_TYPE_FLAVOR   = 1;
    public static final int CATEGORY_TYPE_BRAND    = 2;
    
    // Product type
    public static final int PRODUCT_TYPE_WINE          = 0;
    public static final int PRODUCT_TYPE_COMPLEMENTARY = 1;
    
    // Ticket status
    public static final int TICKET_STATUS_PENDING  = 0;
    public static final int TICKET_STATUS_ON_HOLD  = 1;
    public static final int TICKET_STATUS_RESOLVED = 2;
    
    // Status
    public static final int STATUS_SUCCESSFUL       = 0;
    public static final int STATUS_FAILED           = 1;
    public static final int STATUS_UNEXPECTED_ERROR = 2;
    public static final int STATUS_WRONG_PASSWORD   = 3;
    public static final int STATUS_NOT_UNIQUE       = 4;
    public static final int STATUS_NOT_FOUND        = 5;
    public static final int STATUS_WRONG_EMAIL      = 6;
    public static final int STATUS_ILLEGAL_STATE    = 7;
    public static final int STATUS_ILLEGAL_INPUT    = 8;
    
    // Email type
    public static final int EMAIL_MEMBER_ACTIVATION    = 1;
    public static final int EMAIL_RECEIPT              = 2;
    public static final int EMAIL_FORGOT               = 3;
    public static final int EMAIL_STAFF_ACTIVATION     = 4;
    public static final int EMAIL_COURSE_END_REMINDER  = 5;
    public static final int EMAIL_LESSONS_CANCELLED    = 6;
    public static final int EMAIL_STAFF_AFTER_LESSON   = 7;
    public static final int EMAIL_STUDENT_AFTER_LESSON = 8;
    public static final int EMAIL_ENQUIRY              = 9;
    public static final int EMAIL_ENQUIRY_AUTO_REPLY   = 10;
    public static final int EMAIL_WELCOME              = 11;
    
    // System constants
    public static final String SYSTEM_MAILBOX_ACCOUNT        = "is4227ultimatum@gmail.com";
    public static final String SYSTEM_MAILBOX_PASSWORD       = "ultimatum";
    public static final String SYSTEM_AMAZON_KEY_ID          = "AKIAJGSP5EPT4EOFEO7Q";
    public static final String SYSTEM_AMAZON_SECRET_KEY      = "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";
    public static final String SYSTEM_AMAZON_SNS_TOPIC_ARN   = "arn:aws:sns:ap-southeast-1:206817212149:WineExpress";
    public static final String SYSTEM_AMAZON_BUCKET_NAME     = "is4227";
    public static final String SYSTEM_AMAZON_S3_PHOTO_FOLDER = "photo/";
    
//    public static final String SYSTEM_CONTENT_DIRECTORY      = "/Users/JamesTran/Desktop/NetBeansProjects/temp/IS4227/";
    public static final String SYSTEM_CONTENT_DIRECTORY      = "/tmp/IS4227/";
}
