/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities.email;

/**
 *
 * @author zilehuang
 */
public class EmailConstants {
    // Email account
    public static final String MAILBOX_ACCOUNT = "wineservicepro@gmail.com";
    public static final String MAILBOX_PASSWORD = "iS4227Project!";
    
    // Auto-reply
    public static final String AUTOREPLY_SUBJECT = "[Auto-Reply] Thank you for your message";
    public static final String AUTOREPLY_CONTENT = "Thank you for your question. <br/>We are currently "
            + "processing your information and we will get back to you as soon "
            + "as possible.";
    
    // Password recovery
    public static final String PASSWORD_SUBJECT = "[Auto-generated] Password Recovery";
    public static final String PASSWORD_CONTENT = "Your temporary password is: ";
}
