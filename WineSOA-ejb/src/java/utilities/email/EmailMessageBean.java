/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities.email;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.mail.AuthenticationFailedException;
import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author zilehuang
 */
@MessageDriven(mappedName = "jms/MessageHandler", activationConfig = {
    @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class EmailMessageBean implements MessageListener {

    public EmailMessageBean() {
    }

    @Override
    public void onMessage(Message message) {
        if (message instanceof ObjectMessage) {
            try {
                this.sendEmail((Email) ((ObjectMessage) message).getObject());
            } catch (JMSException ex) {
                Logger.getLogger(EmailMessageBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void sendEmail(Email email) {
        ArrayList<String> _toList = email.getToList();
        ArrayList<String> _ccList = email.getCcList();
        ArrayList<String> _bccList = email.getBccList();
        ArrayList<String> _imgs;
        String _subject = email.getSubject();
        String _content = email.getContent();
        ArrayList<File> _attachment = email.getAttachment();

        try {
            Properties props = new Properties();
            props.setProperty("mail.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.auth", "true");
            props.setProperty("mail.smtp.starttls.enable", "true");

            Authenticator auth =
                    new EmailMessageBean.SMTPAuthenticator(
                    EmailConstants.MAILBOX_ACCOUNT,
                    EmailConstants.MAILBOX_PASSWORD);

            Session session = Session.getInstance(props, auth);

            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(EmailConstants.MAILBOX_ACCOUNT));
            msg.setSubject(_subject);

            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setContent(_content, "text/html");

            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);

            _imgs = this.imageDetector(_content);

            for (String img : _imgs) {
                MimeBodyPart mbp2 = new MimeBodyPart();
                FileDataSource fds = new FileDataSource(img);
                mbp2.setDataHandler(new DataHandler(fds));
//                String fileName = imgs.substring(imgs.lastIndexOf("/") + 1);
                mbp2.addHeader("Content-ID", "<" + img + ">");
                mbp2.setDisposition(MimeBodyPart.INLINE);
                mbp2.setFileName(fds.getName());
                mp.addBodyPart(mbp2);
            }

            if (_attachment != null) {
                for (File f : _attachment) {
                    MimeBodyPart mbp2 = new MimeBodyPart();
                    FileDataSource fds = new FileDataSource(f);
                    mbp2.setDataHandler(new DataHandler(fds));
                    String fileName = f.getName();
                    mbp2.addHeader("Content-ID", "<" + fileName + ">");
                    mbp2.setDisposition(MimeBodyPart.INLINE);
                    mbp2.setFileName(fds.getName());
                    mp.addBodyPart(mbp2);
                }
            }

            msg.setContent(mp);

            msg.setSentDate(new Date());

            for (String to : _toList) {
                msg.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(to));
            }
            for (String cc : _ccList) {
                msg.addRecipient(javax.mail.Message.RecipientType.CC, new InternetAddress(cc));
            }
            for (String bcc : _bccList) {
                msg.addRecipient(javax.mail.Message.RecipientType.BCC, new InternetAddress(bcc));
            }

            Transport.send(msg);

        } catch (AuthenticationFailedException ex) {
        } catch (AddressException ex) {
        } catch (MessagingException ex) {
        }
    }

    private ArrayList<String> imageDetector(String content) {
        ArrayList<String> imgs = new ArrayList<String>();
        int start = -1;
        int end = -1;
        while ((start = content.indexOf("<img src=\"cid:")) != -1) {
            content = content.substring(start + 14);
            end = content.indexOf("\"");
            imgs.add(content.substring(0, end));
        }
        return imgs;
    }

    private class SMTPAuthenticator extends Authenticator {

        private PasswordAuthentication authentication;

        public SMTPAuthenticator(String login, String PASSWORD) {
            authentication = new PasswordAuthentication(login, PASSWORD);
        }

        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
            return authentication;
        }
    }
}
