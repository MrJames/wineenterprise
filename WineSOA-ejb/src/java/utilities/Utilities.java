/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.io.*;
import java.text.DecimalFormat;

/**
 *
 * @author James
 */
public class Utilities {
    
    public static boolean mkDir(String dirName) {
        File file = new File(dirName);
        return file.mkdir();
    }
    
    public static void writeToDisk(String fileName, byte[] content) throws IOException {
        try {
            File file = new File(Constants.SYSTEM_CONTENT_DIRECTORY + fileName);
            FileOutputStream fileOuputStream = new FileOutputStream(file);
            fileOuputStream.write(content);
            fileOuputStream.close();
        } catch (Exception e) {
            System.out.println("WRITE DISK " + e.toString());
        }
    }
    
    public static double roundTwoDecimals(double d) {
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        return Double.valueOf(twoDForm.format(d));
    }
}
