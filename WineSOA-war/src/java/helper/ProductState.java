/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author user
 */
public class ProductState implements Serializable{
    private Long    id;
    private String  name;
    private String  description;
    private String  photo;
    private Double  cost;
    private Double  price;
    private Integer type;
    private Integer quantity;
    private Integer totalRating;
    private Integer numberOfReviewers;
    private boolean deleted;
    private Long    createdDate;
    private Set<CategoryState>     categories;
    private List<ProductInfoState> productInfo;
    private List<ReviewState>      reviews;

    public ProductState() {
        this.categories = new HashSet();
    }
    
    public void addCategory(CategoryState cs) {
        this.categories.add(cs);
    }
    
    // Getters and Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getTotalRating() {
        return totalRating;
    }

    public void setTotalRating(Integer totalRating) {
        this.totalRating = totalRating;
    }

    public Integer getNumberOfReviewers() {
        return numberOfReviewers;
    }

    public void setNumberOfReviewers(Integer numberOfReviewers) {
        this.numberOfReviewers = numberOfReviewers;
    }

    public Set<CategoryState> getCategories() {
        return categories;
    }

    public void setCategories(Set<CategoryState> categories) {
        this.categories = categories;
    }

    public List<ProductInfoState> getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(List<ProductInfoState> productInfo) {
        this.productInfo = productInfo;
    }

    public List<ReviewState> getReviews() {
        return reviews;
    }

    public void setReviews(List<ReviewState> reviews) {
        this.reviews = reviews;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProductState other = (ProductState) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
}
