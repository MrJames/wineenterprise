/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author JamesTran
 */
public class Cart implements Serializable {
    private double        totalCost;
    private Set<CartItem> items;
    
    public Cart() {
        this.totalCost = 0;
        this.items     = new HashSet();
    }
    
    public void clearCart() {
        this.items.clear();
        this.totalCost = 0;
    }
    
    public boolean addItem(ProductState product) {
        CartItem ci = new CartItem(product, this);
        boolean unique = this.items.add(ci);
        if (unique) {
            this.totalCost += ci.getTotalCost();
            return true;
            
        } else return false;
    }
    
    public void updateTotalCost(double amountToUpdate) {
        this.totalCost += amountToUpdate;
    }
    
    public void removeItem(CartItem ci) {
        boolean existing = this.items.remove(ci);
        if (existing) this.totalCost -= ci.getTotalCost();
    }
    
    // Getters and Setters
    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public Set<CartItem> getItems() {
        return items;
    }

    public void setItems(Set<CartItem> items) {
        this.items = items;
    }
}
