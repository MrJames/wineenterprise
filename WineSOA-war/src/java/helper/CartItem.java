/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author JamesTran
 */
public class CartItem implements Serializable {
    private int          quantity;
    private ProductState product;
    private double       totalCost;
    private Cart         cart;

    public CartItem(ProductState product, Cart cart) {
        this.product   = product;
        this.quantity  = 1;
        this.totalCost = product.getPrice();
        this.cart      = cart;
    }

    // Getters and Setters
    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public ProductState getProduct() {
        return product;
    }

    public void setProduct(ProductState product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
        
        // Re-calculate totalCost for this item and the cart
        double oldTotalCost = this.totalCost;
        this.totalCost = product.getPrice() * quantity;
        this.cart.updateTotalCost(this.totalCost - oldTotalCost);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.product);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CartItem other = (CartItem) obj;
        if (!Objects.equals(this.product, other.product)) {
            return false;
        }
        return true;
    }
}
