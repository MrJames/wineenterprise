/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author user
 */
public class TransactionsState implements Serializable{
    private Long      id;
    private Long      createdDate;
    private Double    totalAmount;
    private String    shippingAddress1;
    private String    shippingAddress2;
    private String    paypalID;
    private UserState customer;
    private List<TransactionItemState> transactionItems;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getShippingAddress1() {
        return shippingAddress1;
    }

    public void setShippingAddress1(String shippingAddress1) {
        this.shippingAddress1 = shippingAddress1;
    }

    public String getShippingAddress2() {
        return shippingAddress2;
    }

    public void setShippingAddress2(String shippingAddress2) {
        this.shippingAddress2 = shippingAddress2;
    }

    public String getPaypalID() {
        return paypalID;
    }

    public void setPaypalID(String paypalID) {
        this.paypalID = paypalID;
    }

    public List<TransactionItemState> getTransactionItems() {
        return transactionItems;
    }

    public void setTransactionItems(List<TransactionItemState> transactionItems) {
        this.transactionItems = transactionItems;
    }

    public UserState getCustomer() {
        return customer;
    }

    public void setCustomer(UserState customer) {
        this.customer = customer;
    }
}
