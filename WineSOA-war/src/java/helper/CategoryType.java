/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import java.io.Serializable;

/**
 *
 * @author JamesTran
 */
public class CategoryType implements Serializable {
    private Integer code;
    private String  name;
    
    public CategoryType(Integer code, String name) {
        this.code = code;
        this.name = name;
    }
    
    // Getters and Setters
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
