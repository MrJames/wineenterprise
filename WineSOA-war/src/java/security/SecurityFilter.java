/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package security;

import java.io.IOException;
import java.util.ResourceBundle;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utilities.WebConstants;

/**
 *
 * @author Belinda
 */
public class SecurityFilter implements Filter {
 
    private FilterConfig   filterConfig;
    private ResourceBundle rb;
    
    @Override
    public void init(FilterConfig filterConfig)
            throws ServletException {
        this.filterConfig = filterConfig;
        this.rb   = ResourceBundle.getBundle("utilities.constants");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest  httpServletRequest  = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        HttpSession         httpSession         = httpServletRequest.getSession(true);
        String              requestServletPath  = httpServletRequest.getServletPath();
        
        if (excludeLoginCheck(requestServletPath)) chain.doFilter(request, response);
        else {
            if (requestServletPath.contains("/admin/")) {
                Object isAdminLogin = httpSession.getAttribute("isAdminLogin");
                if (isAdminLogin == null || isAdminLogin.equals(Boolean.FALSE)) {
                    httpServletResponse.sendRedirect(WebConstants.SYSTEM_MEMBER_PATH + "index.xhtml");
                    return;
                }
            
            } else if (requestServletPath.contains("/member/")) {
                Object isMemberLogin = httpSession.getAttribute("isMemberLogin");
                Object isAdminLogin  = httpSession.getAttribute("isAdminLogin");
                
                if ((isMemberLogin == null || isMemberLogin.equals(Boolean.FALSE))
                        && (isAdminLogin == null || isAdminLogin.equals(Boolean.FALSE))) {
                    httpServletResponse.sendRedirect(WebConstants.SYSTEM_MEMBER_PATH + "index.xhtml");
                    return;
                }
            }
            
            // Forward file uploading requests
            if (requestServletPath.equals("/admin/CreateProduct.xhtml")) {
                request.getRequestDispatcher(requestServletPath).forward(request, response);

            } else chain.doFilter(request, response);
        }
    }

    private Boolean excludeLoginCheck(String path) {
        if (path.contains("/static/")
                || path.startsWith("/javax.faces.resource")
                || path.equals("/member/test.xhtml")
                
                || path.equals("/admin/ActivateAdmin.xhtml")
                
                || path.equals("/member/index.xhtml")
                || path.equals("/member/ViewCart.xhtml")
                || path.equals("/member/ConfirmOrder.xhtml")
                || path.equals("/member/SearchPage.xhtml")
                || path.equals("/member/Login.xhtml")
                || path.equals("/member/LoginStatus.xhtml")
                || path.equals("/member/ActivateAccount.xhtml")
                || path.equals("/member/AccountRecovery.xhtml")
                || path.equals("/member/ContactUs.xhtml")
                || path.equals("/member/RegisterAccount.xhtml")
                || path.equals("/member/Contact.xhtml")
                || path.equals("/member/ViewProduct.xhtml")
                ) {
            return true;
        } else return false;
    }

    @Override
    public void destroy() {
    
    }
}