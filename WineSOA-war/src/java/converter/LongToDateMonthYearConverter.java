/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 *
 * @author JamesTran
 */
@FacesConverter("converter.longToDateMonthYearConverter")
public class LongToDateMonthYearConverter implements Converter {
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return value;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        DateTime date = new DateTime((Long) value, DateTimeZone.forID("Asia/Shanghai"));
        return date.toString("MMM dd, yyyy");
    }
    
}
