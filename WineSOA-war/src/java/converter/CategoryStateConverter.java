/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package converter;

import helper.CategoryState;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author JamesTran
 */
@FacesConverter("converter.categoryStateConverter")
public class CategoryStateConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.trim().isEmpty()) {
            return null;
        } else {
            System.out.println("CONVERTER: " + value);
            return new CategoryState(Long.valueOf(value));
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((CategoryState) value).getId());
        }
    }
}
