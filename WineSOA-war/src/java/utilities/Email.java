package utilities;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

public class Email implements Serializable {

    private ArrayList<String> toList = new ArrayList<String>();
    private ArrayList<String> ccList = new ArrayList<String>();
    private ArrayList<String> bccList = new ArrayList<String>();
    private String subject = new String();
    private String content = new String();
    private ArrayList<File> attachment = null;

    public Email() {
    }

    public Email(
            ArrayList<String> toList,
            ArrayList<String> ccList,
            ArrayList<String> bccList,
            String subject,
            String content,
            ArrayList<File> attachment) {
        this.toList = toList;
        this.ccList = ccList;
        this.bccList = bccList;
        this.subject = subject;
        this.content = content;
        this.attachment = attachment;
    }

    public boolean isEmpty() {
        if (this.content.isEmpty() && (this.attachment == null || this.attachment.isEmpty())) {
            return true;
        } else {
            return false;
        }
    }

    public ArrayList<String> getToList() {
        return toList;
    }

    public void setToList(ArrayList<String> toList) {
        this.toList = toList;
    }

    public ArrayList<String> getCcList() {
        return ccList;
    }

    public void setCcList(ArrayList<String> ccList) {
        this.ccList = ccList;
    }

    public ArrayList<String> getBccList() {
        return bccList;
    }

    public void setBccList(ArrayList<String> bccList) {
        this.bccList = bccList;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ArrayList<File> getAttachment() {
        return attachment;
    }

    public void setAttachment(ArrayList<File> attachment) {
        this.attachment = attachment;
    }
}
