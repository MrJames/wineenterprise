/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 *
 * @author JamesTran
 */
public class ServiceConstants {
    // Category type
    public static final int CATEGORY_TYPE_REGION   = 0;
    public static final int CATEGORY_TYPE_FLAVOR   = 1;
    public static final int CATEGORY_TYPE_BRAND    = 2;
    
    // Product type
    public static final int PRODUCT_TYPE_WINE          = 0;
    public static final int PRODUCT_TYPE_COMPLEMENTARY = 1;
    
    // Ticket status
    public static final int TICKET_STATUS_PENDING  = 0;
    public static final int TICKET_STATUS_ON_HOLD  = 1;
    public static final int TICKET_STATUS_RESOLVED = 2;
    
    // Status
    public static final int STATUS_SUCCESSFUL       = 0;
    public static final int STATUS_FAILED           = 1;
    public static final int STATUS_UNEXPECTED_ERROR = 2;
    public static final int STATUS_WRONG_PASSWORD   = 3;
    public static final int STATUS_NOT_UNIQUE       = 4;
    public static final int STATUS_NOT_FOUND        = 5;
    public static final int STATUS_WRONG_EMAIL      = 6;
    public static final int STATUS_ILLEGAL_STATE    = 7;
    public static final int STATUS_ILLEGAL_INPUT    = 8;
}
