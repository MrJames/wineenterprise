/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 *
 * @author JamesTran
 */
public class WebConstants {
    // System variable
//    public static final String SYSTEM_APP_DOMAIN           = "http://localhost:8080";
//    public static final String SYSTEM_MEMBER_PATH          = "http://localhost:8080/wine/member/";
//    public static final String SYSTEM_MEMBER_SSL_PATH      = "https://localhost:8181/wine/member/";
//    public static final String SYSTEM_ADMIN_PATH           = "http://localhost:8080/wine/admin/";
//    public static final String SYSTEM_ADMIN_SSL_PATH       = "https://localhost:8181/wine/admin/";
//    public static final String SYSTEM_FACEBOOK_APP_ID      = "372208722888225";
//    public static final String SYSTEM_FACEBOOK_APP_SECRET  = "8e0b33e74d8116a438e43d1917f42127";
    
    
    public static final String SYSTEM_APP_DOMAIN           = "http://ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com";
    public static final String SYSTEM_MEMBER_PATH          = "http://ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/wine/member/";
    public static final String SYSTEM_MEMBER_SSL_PATH      = "https://ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/wine/member/";
    public static final String SYSTEM_ADMIN_PATH           = "http://ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/wine/admin/";
    public static final String SYSTEM_ADMIN_SSL_PATH       = "https://ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/wine/admin/";
    public static final String SYSTEM_FACEBOOK_APP_ID      = "275422152590426";
    public static final String SYSTEM_FACEBOOK_APP_SECRET  = "f4c673818a3fe74c7bc5c955733b674c";
    
    // PayPal
    public static final String SYSTEM_PAYPAL_BUSINESS_EMAIL  = "james_1312213514_biz@comp.nus.edu.sg";
    public static final String SYSTEM_PAYPAL_CERTIFICATE_ID  = "BZLCH7DBPEWAQ";
    public static final String SYSTEM_PAYPAL_API_USERNAME    = "james_1312213514_biz_api1.comp.nus.edu.sg";
    public static final String SYSTEM_PAYPAL_API_PASSWORD    = "1312213550";
    public static final String SYSTEM_PAYPAL_API_SIGNATURE   = "AFcWxV21C7fd0v3bYYYRCpSSRl31Aq6A1KWWG-eUx753Hp9bHnWZ2sOj";
    public static final String SYSTEM_PAYPAL_APP_ID          = "APP-80W284485P519543T"; // fox Sandbox
    
    // Google analytic
    public static final String GOOGLE_ANALYTIC_USERNAME = "is4227ultimatum@gmail.com";
    public static final String GOOGLE_ANALYTIC_PASSWORD = "ultimatum";
    public static final String GOOGLE_ANALYTIC_TABLE_ID = "ga:71317882";
}
