/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonBeans;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import helper.CategoryState;
import helper.CategoryType;
import helper.ProductType;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.xml.ws.WebServiceRef;
import webServices.ProductServices.ProductServices;
import webServices.ProductServices.ProductServices_Service;
import utilities.ServiceConstants;
import webServices.AccountServices.AccountServices_Service;

/**
 *
 * @author JamesTran
 */
@Named(value = "commonInfo")
@ApplicationScoped
public class CommonInfo {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/ProductServices/ProductServices.wsdl")
    private ProductServices_Service service;
    private ProductServices         port;
    private List<CategoryState>     categories;
    private List<CategoryType>      categoryTypes;
    private List<ProductType>       productTypes;
    
    public CommonInfo() {}
    
    @PostConstruct
    public void prepareService() {
        this.port = service.getProductServicesPort();
        
        // Initiate select item lists
        this.categories = new Gson().fromJson(port.findAllCategories(), new TypeToken<List<CategoryState>>(){}.getType());
        
        this.categoryTypes = new LinkedList();
        categoryTypes.add(new CategoryType(ServiceConstants.CATEGORY_TYPE_REGION, "Region"));
        categoryTypes.add(new CategoryType(ServiceConstants.CATEGORY_TYPE_FLAVOR, "Flavor"));
        categoryTypes.add(new CategoryType(ServiceConstants.CATEGORY_TYPE_BRAND, "Brand"));
        
        this.productTypes = new LinkedList();
        productTypes.add(new ProductType(ServiceConstants.PRODUCT_TYPE_WINE, "Wine"));
        productTypes.add(new ProductType(ServiceConstants.PRODUCT_TYPE_COMPLEMENTARY, "Complementary"));
    }
    
    public void addCategory(CategoryState cs) {
        this.categories.add(cs);
    }
    
    public List<CategoryState> filterCategories(String typeStr) {
        List<CategoryState> filteredCategories = new LinkedList();
        if (typeStr == null || typeStr.isEmpty()) filteredCategories.addAll(categories);
        else {
            Integer type = Integer.valueOf(typeStr);
            for (CategoryState cs : categories) {
                if (cs.getType() == type) filteredCategories.add(cs);
            }
        }
        
        return filteredCategories;
    }
    
    // Getters and Setters
    public ProductServices_Service getService() {
        return service;
    }

    public void setService(ProductServices_Service service) {
        this.service = service;
    }

    public List<CategoryType> getCategoryTypes() {
        return categoryTypes;
    }

    public void setCategoryTypes(List<CategoryType> categoryTypes) {
        this.categoryTypes = categoryTypes;
    }

    public List<ProductType> getProductTypes() {
        return productTypes;
    }

    public void setProductTypes(List<ProductType> productTypes) {
        this.productTypes = productTypes;
    }

    public List<CategoryState> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryState> categories) {
        this.categories = categories;
    }

    public ProductServices getPort() {
        return port;
    }

    public void setPort(ProductServices port) {
        this.port = port;
    }
}
