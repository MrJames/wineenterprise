/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonBeans;

import com.google.gson.Gson;
import com.paypal.wpstoolkit.services.EWPServices;
import helper.Cart;
import helper.CartItem;
import helper.ProductState;
import helper.UserState;
import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.ws.WebServiceRef;
import org.brickred.socialauth.AuthProvider;
import org.brickred.socialauth.Profile;
import org.brickred.socialauth.SocialAuthConfig;
import org.brickred.socialauth.SocialAuthManager;
import org.brickred.socialauth.util.SocialAuthUtil;
import utilities.WebConstants;  
import webServices.AccountServices.AccountServices;
import webServices.AccountServices.AccountServices_Service;

/**
 *
 * @author JamesTran
 */
@Named(value = "userSession")
@SessionScoped
public class UserSessionBean implements Serializable {
//    @Inject
//    private SocialAuth     socialauth;
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/AccountServices/AccountServices.wsdl")
    private AccountServices_Service service;
    private AccountServices         port;
    
    private boolean           socialLogin;
    private SocialAuthManager manager;
    private String            providerID;
    private String            originalURL;
    private String            paypalButton;
    private UserState         us;
    private Cart              cart;
    private ResourceBundle    rb;
    
    /**
     * Creates a new instance of UserSessionBean
     */
    
    public UserSessionBean() { 
        this.rb          = ResourceBundle.getBundle("utilities.constants");
        this.cart        = new Cart();
        this.socialLogin = false;
    }
    
    @PostConstruct
    public void prepareService() {
        this.port = service.getAccountServicesPort();
    }
    
    public String getPaypalButton() {
        try {
            EWPServices ewp = new EWPServices();
            StringBuilder buffer = new StringBuilder("cmd=_cart\n");
            buffer.append("upload=1\n");
            buffer.append("business=" + WebConstants.SYSTEM_PAYPAL_BUSINESS_EMAIL + "\n");
            buffer.append("cert_id=" + WebConstants.SYSTEM_PAYPAL_CERTIFICATE_ID + "\n");
            buffer.append("charset=UTF-8\n");
            
            int i = 1;
            for (CartItem ci : this.cart.getItems()) {
                ProductState product = ci.getProduct();
                buffer.append("item_name_"   + i + "=" + product.getName()  + "\n");
                buffer.append("item_number_" + i + "=" + product.getId()    + "\n");
                buffer.append("amount_"      + i + "=" + product.getPrice() + "\n");
                buffer.append("quantity_"    + i + "=" + ci.getQuantity()   + "\n");
                i++;
            }
            
            buffer.append("currency_code=" + "SGD" + "\n");
            buffer.append("no_shipping="   + "1" + "\n");
            buffer.append("return="        + WebConstants.SYSTEM_MEMBER_PATH + "index.xhtml" + "\n");
            buffer.append("cancel_return=" + WebConstants.SYSTEM_MEMBER_PATH + "index.xhtml" + "\n");
            buffer.append("notify_url="    + WebConstants.SYSTEM_MEMBER_PATH + "ConfirmOrder.xhtml" + "\n");
            buffer.append("custom="        + this.us.getEmail() + "\n");
            
            String str = buffer.toString();
            String envURL= "https://www.sandbox.paypal.com";
            paypalButton = ewp.encryptButton(str.getBytes("UTF-8"), FacesContext.getCurrentInstance().getExternalContext().getRealPath("WEB-INF/cert/paypal-p12.p12"), "lingua123@", FacesContext.getCurrentInstance().getExternalContext().getRealPath("WEB-INF/cert/sandbox_cert_pem.txt"), envURL, "https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif");
            
            return paypalButton;
        } catch (Exception ex) {
            System.out.println("UserSession - PayPal - Exception: " + ex.toString());
            return null;
        }
    }
    
    public void addCartItem(ProductState product) {
        if (this.cart.addItem(product)) {
            FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_INFO, 
                    "Successful", "You have succesfully put this item in your cart."));
        } else {
            FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    "Failed", "You have already put this item in your cart."));
        }
    }
    
    public void pullUserInfo() {
        try {
            this.socialLogin = true;
            
            // Pull user's data from the provider
            ExternalContext    externalContext = FacesContext.getCurrentInstance().getExternalContext();
            HttpServletRequest request         = (HttpServletRequest) externalContext.getRequest();
            Map                map             = SocialAuthUtil.getRequestParametersMap(request);
            if (this.manager != null) {
                AuthProvider provider = manager.connect(map);
                Profile profile = provider.getUserProfile();

                // Do what you want with the data (e.g. persist to the database, etc.)
                String jsonizedObject = port.login2(profile.getEmail());
                this.us               = new Gson().fromJson(jsonizedObject, UserState.class);
                if (us == null) {
                    jsonizedObject = port.registerMember2(profile.getEmail(), profile.getFirstName(), profile.getLastName(), profile.getGender());
                    this.us        = new Gson().fromJson(jsonizedObject, UserState.class);
                
                } else if (us.isBlocked()) {
                    // Disconnect from the provider
                    manager.disconnectProvider(providerID);
                    this.manager = null;
                    this.us      = null;
                    
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                            "Failed", "Oops! Your account has been blocked by our admin.<br/><br/>Please click <a href=\"" + WebConstants.SYSTEM_MEMBER_PATH + "ContactUs.xhtml\">here</a> to contact us if you don't know why you are blocked."));
                
                } else if (us.isDeleted()) {
                    // Disconnect from the provider
                    manager.disconnectProvider(providerID);
                    this.manager = null;
                    this.us      = null;
                    
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                            "Failed", "Oops! This account has been deleted."));
                    
                } else {
                    // Retrieve user's photo URL
                    this.us.setProfileImageURL(profile.getProfileImageURL());

                    // Save user session
                    if (this.us.isIsAdmin()) this.saveAdminSession(request);
                    else this.saveMemberSession(request);

                    // Redirect the user back to where they have been before logging in
                    externalContext.redirect(originalURL);
                
                }
                
            } else externalContext.redirect(WebConstants.SYSTEM_MEMBER_PATH + "index.xhtml");
            
        } catch (Exception ex) {
            System.out.println("UserSessionBean - Exception: " + ex.toString());
        }
    }
    
    public void socialConnect() throws Exception {
        // Put your keys and secrets from the providers here 
        Properties props = System.getProperties();
        props.put("graph.facebook.com.consumer_key", WebConstants.SYSTEM_FACEBOOK_APP_ID);
        props.put("graph.facebook.com.consumer_secret", WebConstants.SYSTEM_FACEBOOK_APP_SECRET);
        
        // Initiate required components
        SocialAuthConfig config = SocialAuthConfig.getDefault();
        config.load(props);
        manager = new SocialAuthManager();
        manager.setSocialAuthConfig(config);
        
        // 'successURL' is the page you'll be redirected to on successful login
        String url = manager.getAuthenticationUrl(providerID, WebConstants.SYSTEM_MEMBER_PATH + "LoginStatus.xhtml?socialLogin=true");
        FacesContext.getCurrentInstance().getExternalContext().redirect(url);
    }
    
    public void logOut() {
        try {
            this.socialLogin = false;
            
            // Disconnect from the provider
            manager.disconnectProvider(providerID);
            
            // Invalidate session
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            this.invalidateSession(request);
            FacesContext.getCurrentInstance().getExternalContext().redirect(rb.getString("SYSTEM_MEMBER_PATH") + "index.xhtml");
        
        } catch (IOException ex) {
            System.out.println("UserSessionBean - IOException: " + ex.toString());
        }
    }
    
    public void search(String keyWord) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(rb.getString("SYSTEM_MEMBER_PATH") + "SearchPage.xhtml?keyWord=" + keyWord);
        
        } catch (IOException ex) {
            System.out.println("UserSessionBean - IOException: " + ex.toString());
        }
    }
    
    public void searchByCategory(Long brandID, Long regionID, Long flavorID) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(rb.getString("SYSTEM_MEMBER_PATH") 
                    + "SearchPage.xhtml?byCategory=true" 
                    + "&brand="  + brandID
                    + "&region=" + regionID
                    + "&flavor=" + flavorID);
        
        } catch (IOException ex) {
            System.out.println("UserSessionBean - IOException: " + ex.toString());
        }
    }
    
    public void saveMemberSession(HttpServletRequest request) {
        HttpSession clientSession = request.getSession();
        clientSession.setMaxInactiveInterval(86400);
        clientSession.setAttribute("isAdminLogin", false);
        clientSession.setAttribute("isMemberLogin", true);
    }
    
    public void saveAdminSession(HttpServletRequest request) {
        HttpSession clientSession = request.getSession();
        clientSession.setMaxInactiveInterval(86400);
        clientSession.setAttribute("isAdminLogin", true);
        clientSession.setAttribute("isMemberLogin", false);
    }
    
    public void invalidateSession(HttpServletRequest request) {
        HttpSession clientSession = request.getSession();
        clientSession.invalidate();
    }
    
    // Getters and Setters
    public String getOriginalURL() {
        return originalURL;
    }

    public void setOriginalURL(String originalURL) {
        this.originalURL = originalURL;
    }

    public boolean isSocialLogin() {
        return socialLogin;
    }

    public void setSocialLogin(boolean socialLogin) {
        this.socialLogin = socialLogin;
    }

    public void setPaypalButton(String paypalButton) {
        this.paypalButton = paypalButton;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public AccountServices_Service getService() {
        return service;
    }

    public void setService(AccountServices_Service service) {
        this.service = service;
    }

    public AccountServices getPort() {
        return port;
    }

    public void setPort(AccountServices port) {
        this.port = port;
    }

    public UserState getUs() {
        return us;
    }

    public void setUs(UserState us) {
        this.us = us;
    }

    public SocialAuthManager getManager() {
        return manager;
    }

    public void setManager(SocialAuthManager manager) {
        this.manager = manager;
    }

    public String getProviderID() {
        return providerID;
    }

    public void setProviderID(String providerID) {
        this.providerID = providerID;
    }
}
