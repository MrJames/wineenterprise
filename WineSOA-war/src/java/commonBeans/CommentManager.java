/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonBeans;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import helper.ReviewState;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.xml.ws.WebServiceRef;
import webServices.ProductServices.ProductServices;
import webServices.ProductServices.ProductServices_Service;
import utilities.ServiceConstants;

/**
 *
 * @author JamesTran
 */
@ManagedBean(name = "commentManager")
@ViewScoped
public class CommentManager {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/ProductServices/ProductServices.wsdl")
    private ProductServices_Service service;
    private ProductServices         port;
    @Inject
    private UserSessionBean   userSession;
    
    private Long              productID;
    private Integer           rating;
    private Integer           firstCommentPosition;
    private String            comment;
    private List<ReviewState> reviews;
    
    public CommentManager() {}
    
    @PostConstruct
    public void prepareService() {
        this.port = service.getProductServicesPort();
        String idStr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("productID");
        if (idStr != null) {
            productID = Long.valueOf(idStr);
            firstCommentPosition = 0;
            this.loadReviews(firstCommentPosition);
        }
    }
    
    public void loadReviews(Integer firstPosition) {
        String jsonizedReviews = port.getReviews(productID, firstPosition);
        List<ReviewState> result = new Gson().fromJson(jsonizedReviews, new TypeToken<List<ReviewState>>(){}.getType());
        if (!result.isEmpty()) {
            this.firstCommentPosition = firstPosition;
            this.reviews              = result;
        }
    }
    
    public void postComment() {
        int result = port.addReview(productID, userSession.getUs().getEmail(), comment, rating);
        
        if (result == ServiceConstants.STATUS_NOT_FOUND) {
            FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    "Failed", "This user does not exist."));
        
        } else if (result == ServiceConstants.STATUS_ILLEGAL_STATE) {
            FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    "Failed", "This user was blocked."));
       
        } else if (result == ServiceConstants.STATUS_FAILED) {
            FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    "Failed", "You cannot review this product until you bought it."));
        
        } else {
            FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_INFO, 
                    "Successful", "You have successfully added a review."));
            this.loadReviews(0);
        }
    }
    
    // Getters and Setters
    public ProductServices_Service getService() {
        return service;
    }

    public void setService(ProductServices_Service service) {
        this.service = service;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public UserSessionBean getUserSession() {
        return userSession;
    }

    public void setUserSession(UserSessionBean userSession) {
        this.userSession = userSession;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ProductServices getPort() {
        return port;
    }

    public void setPort(ProductServices port) {
        this.port = port;
    }

    public Long getProductID() {
        return productID;
    }

    public void setProductID(Long productID) {
        this.productID = productID;
    }

    public Integer getFirstCommentPosition() {
        return firstCommentPosition;
    }

    public void setFirstCommentPosition(Integer firstCommentPosition) {
        this.firstCommentPosition = firstCommentPosition;
    }

    public List<ReviewState> getReviews() {
        return reviews;
    }

    public void setReviews(List<ReviewState> reviews) {
        this.reviews = reviews;
    }
}
