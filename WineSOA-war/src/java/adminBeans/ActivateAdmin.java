/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adminBeans;

import com.google.gson.Gson;
import commonBeans.UserSessionBean;
import helper.UserState;
import java.util.Map;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceRef;
import org.brickred.socialauth.AuthProvider;
import org.brickred.socialauth.Profile;
import org.brickred.socialauth.SocialAuthConfig;
import org.brickred.socialauth.SocialAuthManager;
import org.brickred.socialauth.util.SocialAuthUtil;
import utilities.WebConstants;
import webServices.AccountServices.AccountServices;
import webServices.AccountServices.AccountServices_Service;

/**
 *
 * @author JamesTran
 */
@Named(value = "activateAdmin")
@RequestScoped
public class ActivateAdmin {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/AccountServices/AccountServices.wsdl")
    private AccountServices_Service service;
    private AccountServices         port;
    @Inject
    private UserSessionBean userSession;
    
    public ActivateAdmin() {}
    
    @PostConstruct
    public void prepareService() {
        this.port = service.getAccountServicesPort();
    }
    
    public void socialConnect(String providerID) throws Exception {
        // Put your keys and secrets from the providers here 
        Properties props = System.getProperties();
        props.put("graph.facebook.com.consumer_key", "372208722888225");
        props.put("graph.facebook.com.consumer_secret", "8e0b33e74d8116a438e43d1917f42127");
        props.put("www.google.com.consumer_key", "1057990753233.apps.googleusercontent.com");
        props.put("www.google.com.consumer_secret", "kp9f9NeMr8fQLr0KOg8orXTr");
        
        // Initiate required components
        SocialAuthConfig config = SocialAuthConfig.getDefault();
        config.load(props);
        SocialAuthManager manager = new SocialAuthManager();
        manager.setSocialAuthConfig(config);
        
        // Remember the manager & providerID
        userSession.setManager(manager);
        userSession.setProviderID(providerID);
        
        // 'successURL' is the page you'll be redirected to on successful login
        String url = manager.getAuthenticationUrl(providerID, WebConstants.SYSTEM_ADMIN_PATH + "ActivateAdmin.xhtml");
        FacesContext.getCurrentInstance().getExternalContext().redirect(url);
    }
    
    public void activateAdmin() {
        try {
            // Pull user's data from the provider
            HttpServletRequest request  = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            Map                map      = SocialAuthUtil.getRequestParametersMap(request);
            SocialAuthManager  manager  = userSession.getManager();
            
            if (manager != null) {
                AuthProvider       provider = manager.connect(map);
                Profile profile = provider.getUserProfile();

                // Do what you want with the data (e.g. persist to the database, etc.)
                String jsonizedObject = port.activateAdmin(profile.getEmail(), profile.getFirstName(), profile.getLastName(), profile.getGender());
                UserState us          = new Gson().fromJson(jsonizedObject, UserState.class);
                if (us == null) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Wrong email", "Oops! No admin account was registered with this email address: " + profile.getEmail() + "."));
                    manager.disconnectProvider(userSession.getProviderID());
                    return ;

                } else {
                    // Remember user's info
                    us.setProfileImageURL(profile.getProfileImageURL());
                    userSession.setUs(us);

                    // Save user session
                    if (us.isIsAdmin()) userSession.saveAdminSession(request);
                    else userSession.saveMemberSession(request);
                }

                // Redirect the admin back to MemberArea
                FacesContext.getCurrentInstance().getExternalContext().redirect(WebConstants.SYSTEM_MEMBER_PATH + "MemberArea.xhtml");
            
            } else FacesContext.getCurrentInstance().getExternalContext().redirect(WebConstants.SYSTEM_MEMBER_PATH + "index.xhtml");
            
        } catch (Exception ex) {
            System.out.println("UserSession - Exception: " + ex.toString());
        }
    }
    
    // Getters and Setters
    public UserSessionBean getUserSession() {
        return userSession;
    }

    public void setUserSession(UserSessionBean userSession) {
        this.userSession = userSession;
    }

    public AccountServices_Service getService() {
        return service;
    }

    public void setService(AccountServices_Service service) {
        this.service = service;
    }

    public AccountServices getPort() {
        return port;
    }

    public void setPort(AccountServices port) {
        this.port = port;
    }
}
