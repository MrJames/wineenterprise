/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adminBeans;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author JamesTran
 */
@Named(value = "mrsBean")
@RequestScoped
public class MrsBean {
    private String input;
    private String testString;
    
    public MrsBean() { }
    
    public void printId(int id) {
        System.out.println("ID is: " + id);
    }
    
    public void printString() {
         System.out.println("STRING IS: " + testString);
    }

    public String getTestString() {
        return testString;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public void setTestString(String testString) {
        this.testString = testString;
        System.out.println("STRING IS SET AS: " + testString);
    }
}
