/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adminBeans;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import commonBeans.CommonInfo;
import helper.CategoryState;
import helper.ProductState;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.xml.ws.WebServiceRef;
import webServices.ProductServices.ProductServices;
import webServices.ProductServices.ProductServices_Service;
import utilities.ServiceConstants;

/**
 *
 * @author JamesTran
 */
@ManagedBean(name = "productManager")
@ViewScoped
public class ProductManager {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/ProductServices/ProductServices.wsdl")
    private ProductServices_Service service;
    private ProductServices         port;
    
    @Inject
    private CommonInfo commonInfo;
    
    private CategoryState      brand;
    private CategoryState      region;
    private CategoryState      flavor;
    private ProductState       product;
    private List<ProductState> products;
    private List<ProductState> filteredProducts;
    
    public ProductManager() {}
    
    @PostConstruct
    public void prepareService() {
        this.port             = service.getProductServicesPort();
        this.products         = new Gson().fromJson(port.getAllProducts(), new TypeToken<List<ProductState>>(){}.getType());
        this.filteredProducts = new LinkedList();
        this.filteredProducts.addAll(products);
    }
    
    public void prepareUpdateProduct(ProductState ps) {
        this.product = ps;
        for (CategoryState cs : ps.getCategories()) {
            if (cs.getType() == ServiceConstants.CATEGORY_TYPE_BRAND)         this.brand    = cs;
            else if (cs.getType() == ServiceConstants.CATEGORY_TYPE_REGION)   this.region   = cs;
            else if (cs.getType() == ServiceConstants.CATEGORY_TYPE_FLAVOR) this.flavor = cs;
        }
    }
    
    public void updateProduct() {
        // Prepare categories
        this.product.getCategories().clear();
        this.product.addCategory(brand);
        this.product.addCategory(region);
        this.product.addCategory(flavor);
        
        String jsonizedCategory = new Gson().toJson(product);
        int result = port.updateProduct(jsonizedCategory);
        
        if (result == ServiceConstants.STATUS_SUCCESSFUL) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                    "Successful", "You have successfuly updated the product info."));

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Failed", "This product does not exist."));
            
        }
    }
    
    public void deleteProduct() {
        int result = port.deleteProduct(product.getId());
        if (result == ServiceConstants.STATUS_SUCCESSFUL) {
            this.filteredProducts.remove(product);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                    "Successful", "You have successfuly deleted the product."));

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Failed", "This product does not exist."));
            
        }
    }
    
    public void filterProducts(Long categoryID, String typeStr) {
        this.filteredProducts = new LinkedList();
        
        // Filter by categoryID
        if (categoryID == null || categoryID == 0) this.filteredProducts.addAll(products);
        else {
            for (ProductState ps : products) {
                for (CategoryState cs : ps.getCategories()) {
                    if (cs.getId() == categoryID) {
                        filteredProducts.add(ps);
                        break;
                    }
                }
            }
        }
        
        // Filter by product type
        if (typeStr != null && !typeStr.isEmpty()) {
            Integer type = Integer.valueOf(typeStr);
            
            Iterator<ProductState> iter = filteredProducts.iterator();
            while (iter.hasNext()) {
                ProductState ps = iter.next();
                 if (ps.getType() != type) iter.remove();
            }
        }
        
        for (ProductState ps : products) {
            System.out.println("DESC " + ps.getDescription());
        }
    }
    
    // Getters and Setters
    public ProductServices_Service getService() {
        return service;
    }

    public void setService(ProductServices_Service service) {
        this.service = service;
    }

    public List<ProductState> getFilteredProducts() {
        return filteredProducts;
    }

    public void setFilteredProducts(List<ProductState> filteredProducts) {
        this.filteredProducts = filteredProducts;
    }

    public CategoryState getBrand() {
        return brand;
    }

    public void setBrand(CategoryState brand) {
        this.brand = brand;
    }

    public CategoryState getRegion() {
        return region;
    }

    public void setRegion(CategoryState region) {
        this.region = region;
    }

    public CategoryState getFlavor() {
        return flavor;
    }

    public void setFlavor(CategoryState flavor) {
        this.flavor = flavor;
    }

    public ProductServices getPort() {
        return port;
    }

    public void setPort(ProductServices port) {
        this.port = port;
    }

    public CommonInfo getCommonInfo() {
        return commonInfo;
    }

    public void setCommonInfo(CommonInfo commonInfo) {
        this.commonInfo = commonInfo;
    }

    public ProductState getProduct() {
        return product;
    }

    public void setProduct(ProductState product) {
        this.product = product;
    }

    public List<ProductState> getProducts() {
        return products;
    }

    public void setProducts(List<ProductState> products) {
        this.products = products;
    }
}
