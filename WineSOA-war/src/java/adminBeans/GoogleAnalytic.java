/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adminBeans;

import com.google.gdata.client.analytics.AnalyticsService;
import com.google.gdata.client.analytics.DataQuery;
import com.google.gdata.data.analytics.DataEntry;
import com.google.gdata.data.analytics.DataFeed;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.joda.time.DateTime;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import utilities.WebConstants;

/**
 *
 * @author JamesTran
 */
@ManagedBean(name = "googleAnalytic")
@ViewScoped
public class GoogleAnalytic {

    private Date    startDate;
    private Date    endDate;
    private long    numberOfVisitors;
    private long    numberOfUniqueVisitors;
    private int     max;
    private boolean renderChart;
    private AnalyticsService    analyticsService;
    private CartesianChartModel googleAnalyticModel; 
    
    public GoogleAnalytic() {
        this.renderChart = false;
    }
    
    @PostConstruct
    public void prepareService() {
        try {
            this.analyticsService = new AnalyticsService("WineExpress");
            this.analyticsService.setUserCredentials(WebConstants.GOOGLE_ANALYTIC_USERNAME, WebConstants.GOOGLE_ANALYTIC_PASSWORD);
              
        } catch (AuthenticationException ex) {
            System.out.println("GoogleAnalytic - AuthenticationException: " + ex.toString());
        }
    }
    
    public void pullData() {
        try {
            this.renderChart = true;
            DateTime start   = new DateTime(startDate);
            DateTime end     = new DateTime(endDate);
            
            // Number of visitors
            // Number of unique visitors
            DataQuery query1 = new DataQuery(new URL(
                  "https://www.google.com/analytics/feeds/data"));
            query1.setStartDate(start.toString("yyyy-MM-dd"));
            query1.setEndDate(end.toString("yyyy-MM-dd"));
            query1.setMetrics("ga:visitors, ga:newVisits");
            query1.setIds(WebConstants.GOOGLE_ANALYTIC_TABLE_ID);

            DataFeed  visitorFeed       = analyticsService.getFeed(query1.getUrl(), DataFeed.class);
            DataEntry visitorEntry      = visitorFeed.getEntries().get(0);
            this.numberOfVisitors       = visitorEntry.longValueOf("ga:visitors");
            this.numberOfUniqueVisitors = visitorEntry.longValueOf("ga:newVisits");

            // Top 10 page view for a period
            DataQuery query2 = new DataQuery(new URL(
                  "https://www.google.com/analytics/feeds/data"));
            query2.setStartDate(start.toString("yyyy-MM-dd"));
            query2.setEndDate(end.toString("yyyy-MM-dd"));
            query2.setDimensions("ga:pageTitle");
            query2.setMetrics("ga:pageviews, ga:uniquePageviews");
            query2.setSort("-ga:pageviews, -ga:uniquePageviews");
            query2.setMaxResults(10);
            query2.setIds(WebConstants.GOOGLE_ANALYTIC_TABLE_ID);

            // Populate chart
            this.googleAnalyticModel = new CartesianChartModel();
            
            ChartSeries pageViewsSeries = new ChartSeries();
            pageViewsSeries.setLabel("Page view");
            
            ChartSeries uniquePageViewsSeries = new ChartSeries();
            uniquePageViewsSeries.setLabel("Unique view");
            
            this.max = 0;
            DataFeed pageFeed = analyticsService.getFeed(query2.getUrl(), DataFeed.class);
            for (DataEntry entry : pageFeed.getEntries()) {
                String pageTitle = entry.stringValueOf("ga:pageTitle").replaceFirst("WineExpress - ", "");
                int pageView   = Integer.valueOf(entry.stringValueOf("ga:pageviews"));
                int uniqueView = Integer.valueOf(entry.stringValueOf("ga:uniquePageviews"));
                pageViewsSeries.set(pageTitle, pageView);
                uniquePageViewsSeries.set(pageTitle, uniqueView);
                
                System.out.println("VIEW: " + pageTitle + " " + pageView);
                
                // Process max
                if (pageView > max)   this.max = pageView;
                if (uniqueView > max) this.max = uniqueView;
            }
            
            this.googleAnalyticModel.addSeries(pageViewsSeries);
            this.googleAnalyticModel.addSeries(uniquePageViewsSeries);
            
        } catch (MalformedURLException ex) {
            System.out.println("GoogleAnalytic - MalformedURLException: " + ex.toString());
            
        } catch (IOException ex) {
            System.out.println("GoogleAnalytic - IOException: " + ex.toString());
            
        } catch (ServiceException ex) {
            System.out.println("GoogleAnalytic - ServiceException: " + ex.toString());
            
        }
    }
    
    // Getters and Setters
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public long getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public long getNumberOfVisitors() {
        return numberOfVisitors;
    }

    public void setNumberOfVisitors(long numberOfVisitors) {
        this.numberOfVisitors = numberOfVisitors;
    }

    public long getNumberOfUniqueVisitors() {
        return numberOfUniqueVisitors;
    }

    public void setNumberOfUniqueVisitors(long numberOfUniqueVisitors) {
        this.numberOfUniqueVisitors = numberOfUniqueVisitors;
    }

    public boolean isRenderChart() {
        return renderChart;
    }

    public void setRenderChart(boolean renderChart) {
        this.renderChart = renderChart;
    }

    public AnalyticsService getAnalyticsService() {
        return analyticsService;
    }

    public void setAnalyticsService(AnalyticsService analyticsService) {
        this.analyticsService = analyticsService;
    }

    public CartesianChartModel getGoogleAnalyticModel() {
        return googleAnalyticModel;
    }

    public void setGoogleAnalyticModel(CartesianChartModel googleAnalyticModel) {
        this.googleAnalyticModel = googleAnalyticModel;
    }
}
