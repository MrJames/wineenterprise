/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adminBeans;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.xml.ws.WebServiceRef;
import utilities.ServiceConstants;
import utilities.WebConstants;
import webServices.AccountServices.AccountServices;
import webServices.AccountServices.AccountServices_Service;

/**
 *
 * @author JamesTran
 */
@Named(value = "registerAdmin")
@RequestScoped
public class RegisterAccount {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/AccountServices/AccountServices.wsdl")
    private AccountServices_Service service;
    private AccountServices         port;

    private String  email;
    private String  password;
    private String  firstName;
    private String  lastName;
    private String  gender;
    /**
     * Creates a new instance of RegisterAccount
     */
    public RegisterAccount() {}
    
    @PostConstruct
    public void prepareService() {
        this.port = service.getAccountServicesPort();
    }
    
    public void validateEmail(FacesContext context, UIComponent toValidate,
            Object value) throws ValidatorException {
        
        String emailStr = (String) value;
        if (emailStr.indexOf("@") == -1) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "This email is invalid.");
            throw new ValidatorException(message);
            
        } else if (port.checkEmailExistence(emailStr) == ServiceConstants.STATUS_NOT_UNIQUE) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "This email has already been registered.");
            throw new ValidatorException(message);
        }
    }
    
    public void register() {
        int result = port.registerAdmin1(email, gender, firstName, lastName, WebConstants.SYSTEM_MEMBER_PATH + "ActivateAccount.xhtml");
        
        if (result == ServiceConstants.STATUS_NOT_UNIQUE) FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Not unique", "Oops! This email has already been registered."));
        else if (result == ServiceConstants.STATUS_SUCCESSFUL) FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Successful", "You have successfuly created an Admin account.<br/>An account activation email has been sent to " + email + ".")); 
    }
    
    // Getters and Setters
    public AccountServices_Service getService() {
        return service;
    }

    public void setService(AccountServices_Service service) {
        this.service = service;
    }

    public AccountServices getPort() {
        return port;
    }

    public void setPort(AccountServices port) {
        this.port = port;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
