/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adminBeans;

import com.google.gson.Gson;
import commonBeans.CommonInfo;
import helper.CategoryState;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.ws.WebServiceRef;
import webServices.ProductServices.ProductServices;
import webServices.ProductServices.ProductServices_Service;

/**
 *
 * @author stringtheory
 */
@Named(value = "createCategory")
@RequestScoped
public class CreateCategory {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/ProductServices/ProductServices.wsdl")
    private ProductServices_Service service;
    private ProductServices         port;
    @Inject
    private CommonInfo commonInfo;
    
    private String  categoryName;
    private Integer categoryType;
    
    public CreateCategory() {}

    @PostConstruct
    public void prepareService() {
        this.port = service.getProductServicesPort();
    }
    
    public void createCategory() {
        String jsonizedCategory = port.createCategory(categoryName, categoryType);
        
        if (jsonizedCategory == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Failed", "This category already exists."));

        } else {
            commonInfo.getCategories().add(new Gson().fromJson(jsonizedCategory, CategoryState.class));
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                    "Successful", "You have successfuly created the category."));
            
        }
    }

    // Getters and Setters
    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public CommonInfo getCommonInfo() {
        return commonInfo;
    }

    public void setCommonInfo(CommonInfo commonInfo) {
        this.commonInfo = commonInfo;
    }

    public ProductServices_Service getService() {
        return service;
    }

    public void setService(ProductServices_Service service) {
        this.service = service;
    }

    public Integer getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(Integer categoryType) {
        this.categoryType = categoryType;
    }

    public ProductServices getPort() {
        return port;
    }

    public void setPort(ProductServices port) {
        this.port = port;
    }
}
