/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adminBeans;

import com.google.gson.Gson;
import commonBeans.CommonInfo;
import helper.CategoryState;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.xml.ws.WebServiceRef;
import org.primefaces.event.RowEditEvent;
import utilities.ServiceConstants;
import webServices.ProductServices.ProductServices;
import webServices.ProductServices.ProductServices_Service;

/**
 *
 * @author JamesTran
 */
@ManagedBean(name = "categoryManager")
@ViewScoped
public class CategoryManager {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/ProductServices/ProductServices.wsdl")
    private ProductServices_Service service;
    private ProductServices         port;
    @Inject
    private CommonInfo commonInfo;
    
    private CategoryState       category;
    private List<CategoryState> categories;
    
    public CategoryManager() {}
    
    @PostConstruct
    public void prepareService() {
        this.port       = service.getProductServicesPort();
        this.categories = commonInfo.filterCategories(null);
    }
    
    public void filterCategories(String type) {
        this.categories = commonInfo.filterCategories(type);
    }
    
    public void updateCategory(RowEditEvent event) {
        CategoryState cs = (CategoryState) event.getObject();
        String jsonizedCategory = new Gson().toJson(cs);
        
        int result = port.updateCategory(jsonizedCategory);
        if (result == ServiceConstants.STATUS_SUCCESSFUL) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                    "Successful", "You have successfuly updated the category info."));

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Failed", "This category does not exist."));
            
        }
    }
    
    public void deleteCategory() {
        int result = port.deleteCategory(category.getId());
        if (result == ServiceConstants.STATUS_SUCCESSFUL) {
            commonInfo.getCategories().remove(category);
            this.categories.remove(category);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                    "Successful", "You have successfuly deleted the category."));

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Failed", "This category does not exist."));
            
        }
    }
    
    // Getters and Setters
    public ProductServices_Service getService() {
        return service;
    }

    public void setService(ProductServices_Service service) {
        this.service = service;
    }

    public CategoryState getCategory() {
        return category;
    }

    public void setCategory(CategoryState category) {
        this.category = category;
    }

    public CommonInfo getCommonInfo() {
        return commonInfo;
    }

    public void setCommonInfo(CommonInfo commonInfo) {
        this.commonInfo = commonInfo;
    }

    public List<CategoryState> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryState> categories) {
        this.categories = categories;
    }

    public ProductServices getPort() {
        return port;
    }

    public void setPort(ProductServices port) {
        this.port = port;
    }
}
