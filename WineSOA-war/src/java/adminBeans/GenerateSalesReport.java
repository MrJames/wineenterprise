/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adminBeans;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import helper.CategoryState;
import java.util.Date;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.xml.ws.WebServiceRef;
import org.joda.time.DateTime;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import webServices.ReportServices.ReportServices;
import webServices.ReportServices.ReportServices_Service;

/**
 *
 * @author JamesTran
 */
@ManagedBean(name = "generateSalesReport")
@ViewScoped
public class GenerateSalesReport {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/ReportServices/ReportServices.wsdl")
    private ReportServices_Service service;
    private ReportServices         port;
    
    // Constants
    final long MS_ANNUAL  = Long.parseLong("31536000000");
    final long MS_MONTHLY = Long.parseLong("2628000000");
    final long MS_WEEKLY  = Long.parseLong("604800000");
    final long MS_DAILY   = Long.parseLong("86400000");

    final int INTERVAL_YEAR  = 1;
    final int INTERVAL_MONTH = 2;
    final int INTERVAL_WEEK  = 3;
    final int INTERVAL_DAY   = 4;
    
    private CartesianChartModel chartModel;
    private int     intervalType;
    private int     range;
    private Date    startDate;
    private Date    endDate;
    private double  maxY;
    private boolean rendered;
    private boolean revenue;
    private boolean cost;
    private boolean profit;
    private boolean quantity;
    private Long    brand;
    private Long    region;
    private Long    flavor;

    public GenerateSalesReport() { 
        this.rendered = false;
    }

    @PostConstruct
    public void init() {
        this.port     = this.service.getReportServicesPort();
        this.revenue  = false;
        this.cost     = false;
        this.profit   = false;
        this.quantity = false;
    }

    public void generate() {
        System.out.println("REPORT: " + brand);
        System.out.println("REPORT: " + region);
        System.out.println("REPORT: " + flavor);
        
        this.rendered = true;
        this.chartModel = new CartesianChartModel();
        
        // Calculate startDate
        DateTime end = new DateTime(endDate);
        switch (intervalType) {
            case INTERVAL_YEAR:
                this.startDate = end.minusYears(range).toDate();
                break;
            case INTERVAL_MONTH:
                this.startDate = end.minusMonths(range).toDate();
                break;
            case INTERVAL_WEEK:
                this.startDate = end.minusWeeks(range).toDate();
                break;
            case INTERVAL_DAY:
                this.startDate = end.minusDays(range).toDate();
                break;
        }
        
        // Add revenue series
        if (this.revenue) {
            ChartSeries revenueSeries = new ChartSeries();  
            revenueSeries.setLabel("Revenue");
            Map<Object, Number> data = new Gson().fromJson(port.generateRevenueReport(startDate.getTime(), endDate.getTime(), intervalType, brand, region, flavor), new TypeToken<Map<Object, Number>>(){}.getType());
            revenueSeries.setData(data);

            this.chartModel.addSeries(revenueSeries);
        }
        
        // Add cost series
        if (this.cost) {
            ChartSeries costSeries = new ChartSeries();  
            costSeries.setLabel("Cost");
            Map<Object, Number> data = new Gson().fromJson(port.generateCostReport(startDate.getTime(), endDate.getTime(), intervalType, brand, region, flavor), new TypeToken<Map<Object, Number>>(){}.getType());
            costSeries.setData(data);

            this.chartModel.addSeries(costSeries);
        }
        
        // Add profit series
        if (this.profit) {
            ChartSeries profitSeries = new ChartSeries();  
            profitSeries.setLabel("Profit");
            Map<Object, Number> data = new Gson().fromJson(port.generateProfitReport(startDate.getTime(), endDate.getTime(), intervalType, brand, region, flavor), new TypeToken<Map<Object, Number>>(){}.getType());
            profitSeries.setData(data);

            this.chartModel.addSeries(profitSeries);
        }
        
        // Add quantity series
        if (this.quantity) {
            ChartSeries quantitySeries = new ChartSeries();  
            quantitySeries.setLabel("Quantity");
            Map<Object, Number> data = new Gson().fromJson(port.generateQuantityReport(startDate.getTime(), endDate.getTime(), intervalType, brand, region, flavor), new TypeToken<Map<Object, Number>>(){}.getType());
            quantitySeries.setData(data);

            this.chartModel.addSeries(quantitySeries);
        }
    }

    // Getters and Setters
    public CartesianChartModel getChartModel() {
        return chartModel;
    }

    public void setChartModel(CartesianChartModel chartModel) {
        this.chartModel = chartModel;
    }

    public boolean isRevenue() {
        return revenue;
    }

    public void setRevenue(boolean revenue) {
        this.revenue = revenue;
    }

    public Long getBrand() {
        return brand;
    }

    public void setBrand(Long brand) {
        this.brand = brand;
    }

    public Long getRegion() {
        return region;
    }

    public void setRegion(Long region) {
        this.region = region;
    }

    public Long getFlavor() {
        return flavor;
    }

    public void setFlavor(Long flavor) {
        this.flavor = flavor;
    }

    public boolean isCost() {
        return cost;
    }

    public void setCost(boolean cost) {
        this.cost = cost;
    }

    public boolean isProfit() {
        return profit;
    }

    public void setProfit(boolean profit) {
        this.profit = profit;
    }

    public boolean isQuantity() {
        return quantity;
    }

    public void setQuantity(boolean quantity) {
        this.quantity = quantity;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public double getMaxY() {
        this.maxY = 0;
        
        for (ChartSeries series : this.chartModel.getSeries()) {
            for (Map.Entry<Object, Number> entry : series.getData().entrySet()) {
                double value = entry.getValue().doubleValue();
                if (value > this.maxY) this.maxY = value;
            }
        }
        
        return maxY;
    }

    public void setMaxY(double maxY) {
        this.maxY = maxY;
    }

    public int getIntervalType() {
        return intervalType;
    }

    public void setIntervalType(int intervalType) {
        this.intervalType = intervalType;
    }

    public ReportServices_Service getService() {
        return service;
    }

    public void setService(ReportServices_Service service) {
        this.service = service;
    }

    public ReportServices getPort() {
        return port;
    }

    public void setPort(ReportServices port) {
        this.port = port;
    }

    public boolean isRendered() {
        return rendered;
    }

    public void setRendered(boolean rendered) {
        this.rendered = rendered;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}