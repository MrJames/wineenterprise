/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adminBeans;

import com.google.gson.Gson;
import commonBeans.CommonInfo;
import helper.CategoryState;
import helper.ProductState;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.xml.ws.WebServiceRef;
import org.primefaces.model.DefaultUploadedFile;
import webServices.ProductServices.ProductServices;
import webServices.ProductServices.ProductServices_Service;
import utilities.ServiceConstants;

/**
 *
 * @author JamesTran
 */
@Named(value = "createProduct")
@RequestScoped
public class CreateProduct {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/ProductServices/ProductServices.wsdl")
    private ProductServices_Service service;
    private ProductServices         port;
    @Inject
    private CommonInfo commonInfo;
    
    private DefaultUploadedFile photo;
    private DefaultUploadedFile bigPhoto;
    private ProductState  product;
    private CategoryState brand;
    private CategoryState region;
    private CategoryState flavor;

    public CreateProduct() {
        this.product    = new ProductState();
    }

    @PostConstruct
    public void prepareService() {
        this.port = service.getProductServicesPort();
    }
    
    public void createProduct() {
        // Add categories to the product
        this.product.addCategory(brand);
        this.product.addCategory(region);
        this.product.addCategory(flavor);

        String jsonizedProduct  = new Gson().toJson(product);
        String jsonizedPhoto    = new Gson().toJson(photo.getContents());
        String jsonizedBigPhoto = new Gson().toJson(bigPhoto.getContents());
        Integer result = port.createProduct(jsonizedProduct, jsonizedPhoto, jsonizedBigPhoto);
        if (result == ServiceConstants.STATUS_SUCCESSFUL) FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Succesful", "You have successfully created the product"));
    }

    // Getters and Setters
    public DefaultUploadedFile getPhoto() {
        return photo;
    }

    public void setPhoto(DefaultUploadedFile photo) {
        this.photo = photo;
    }

    public DefaultUploadedFile getBigPhoto() {
        return bigPhoto;
    }

    public void setBigPhoto(DefaultUploadedFile bigPhoto) {
        this.bigPhoto = bigPhoto;
    }

    public CommonInfo getCommonInfo() {
        return commonInfo;
    }

    public void setCommonInfo(CommonInfo commonInfo) {
        this.commonInfo = commonInfo;
    }

    public CategoryState getBrand() {
        return brand;
    }

    public void setBrand(CategoryState brand) {
        this.brand = brand;
    }

    public CategoryState getRegion() {
        return region;
    }

    public void setRegion(CategoryState region) {
        this.region = region;
    }

    public CategoryState getFlavor() {
        return flavor;
    }

    public void setFlavor(CategoryState flavor) {
        this.flavor = flavor;
    }

    public ProductServices_Service getService() {
        return service;
    }

    public void setService(ProductServices_Service service) {
        this.service = service;
    }

    public ProductServices getPort() {
        return port;
    }

    public void setPort(ProductServices port) {
        this.port = port;
    }

    public ProductState getProduct() {
        return product;
    }

    public void setProduct(ProductState product) {
        this.product = product;
    }
}
