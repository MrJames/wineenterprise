/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adminBeans;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.PublishRequest;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author JamesTran
 */
@Named(value = "notificationManager")
@RequestScoped
public class NotificationManager {

    private String subject;
    private String content;
    
    public NotificationManager() { }
    
    public void sendNotification() {
        AWSCredentials credentials = new BasicAWSCredentials("AKIAJGSP5EPT4EOFEO7Q", "utpiNviFkhKmbQTYouz5DhwlGK4WEhYuiQNd2LWt");		
        AmazonSNSClient snsClient = new AmazonSNSClient(credentials);
        snsClient.setEndpoint("sns.ap-southeast-1.amazonaws.com");
        
        PublishRequest pr = new PublishRequest("arn:aws:sns:ap-southeast-1:206817212149:WineExpress", content, subject);
        snsClient.publish( pr );
        
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Succesful", "You have successfully pushed the notification to all subscribed users."));
    }
    
    // Getters and Setters
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
