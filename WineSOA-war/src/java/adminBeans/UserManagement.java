/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adminBeans;

import com.google.gson.Gson;
import commonBeans.UserSessionBean;
import helper.UserState;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.xml.ws.WebServiceRef;
import utilities.ServiceConstants;
import webServices.AccountServices.AccountServices;
import webServices.AccountServices.AccountServices_Service;

/**
 *
 * @author JamesTran
 */
@ManagedBean(name = "userManager")
@ViewScoped
public class UserManagement {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/AccountServices/AccountServices.wsdl")
    private AccountServices_Service service;
    private AccountServices         port;
    private ResourceBundle          rb;
    @Inject
    private UserSessionBean userSession;
    private String          adminPassword;
    private String          userEmail;
    private UserState       user;
    
    /**
     * Creates a new instance of UserManagement
     */
    public UserManagement() {}
    
    @PostConstruct
    public void prepareService() {
        this.rb   = ResourceBundle.getBundle("utilities.constants");
        this.port = service.getAccountServicesPort();
    }
    
    public void getUserInfo() {
        this.user = null;
        String jsonizedObject = port.getUserInfo(userEmail);
        
        if (jsonizedObject == null) FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "Oops! No user has registered with this email."));
        else this.user = new Gson().fromJson(jsonizedObject, UserState.class);
    }
    
    public void toggleUserBlockStatus() {
        int result = port.toggleUserBlockStatus(userSession.getUs().getEmail(), adminPassword, user.getEmail());
        
        if (result == ServiceConstants.STATUS_WRONG_PASSWORD) FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "Oops! You have entered wrong password."));
        else if (result == ServiceConstants.STATUS_NOT_FOUND) FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "Oops! No user has registered with this email."));
        else if (result == ServiceConstants.STATUS_SUCCESSFUL) {
            Boolean newBlockStatus = !user.isBlocked();
            user.setBlocked(newBlockStatus);
            
            if (newBlockStatus) FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_INFO, "Successful", "You have successfully blocked this account."));
            else FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_INFO, "Successful", "You have successfully unblocked this account."));
        }
    }
    
    public void deleteUser() {
        int result = port.deleteUser(userSession.getUs().getEmail(), adminPassword, user.getEmail());
        
        if (result == ServiceConstants.STATUS_WRONG_PASSWORD) FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "Oops! You have entered wrong password."));
        else if (result == ServiceConstants.STATUS_NOT_FOUND) FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "Oops! No user has registered with this email."));
        else if (result == ServiceConstants.STATUS_SUCCESSFUL) {
            FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_INFO, "Successful", "You have successfully deleted this account."));
            this.user = null;
        }
    }
    
    // Getters and Setters
    public AccountServices_Service getService() {
        return service;
    }

    public void setService(AccountServices_Service service) {
        this.service = service;
    }

    public AccountServices getPort() {
        return port;
    }

    public void setPort(AccountServices port) {
        this.port = port;
    }

    public UserSessionBean getUserSession() {
        return userSession;
    }

    public void setUserSession(UserSessionBean userSession) {
        this.userSession = userSession;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public UserState getUser() {
        return user;
    }

    public void setUser(UserState user) {
        this.user = user;
    }

    public ResourceBundle getRb() {
        return rb;
    }

    public void setRb(ResourceBundle rb) {
        this.rb = rb;
    }
}
