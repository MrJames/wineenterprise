/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package productReview;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.xml.ws.WebServiceRef;
import webServices.ProductServices.ProductServices;
import webServices.ProductServices.ProductServices_Service;

/**
 *
 * @author Magdelyn
 */
@ManagedBean
@ViewScoped
public class ViewProductRatingManagedBean {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/ProductServices/ProductServices.wsdl")
    private ProductServices_Service service;
    private ProductServices         port;
    
    private Long productId = (long) 2;
    private Double avgRatings;

    public ViewProductRatingManagedBean() {}
    
    @PostConstruct
    public void prepareService() {
        this.port = service.getProductServicesPort();
    }

    public Double showAvgRatings() {
        System.out.println("NPMB:avgRatings is called");
        avgRatings = port.showAvgRating(productId);
        return avgRatings;
    }

    // Getters and Setters
    public void setAvgRatings(Double avgRatings) {
        this.avgRatings = avgRatings;
    }

    public Double getAvgRatings() {
        return avgRatings;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getProductId() {
        return productId;
    }

    public ProductServices_Service getService() {
        return service;
    }

    public void setService(ProductServices_Service service) {
        this.service = service;
    }

    public ProductServices getPort() {
        return port;
    }

    public void setPort(ProductServices port) {
        this.port = port;
    }
}
