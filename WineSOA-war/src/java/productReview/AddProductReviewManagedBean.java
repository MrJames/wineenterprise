/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package productReview;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.xml.ws.WebServiceRef;
import webServices.ProductServices.ProductServices;
import webServices.ProductServices.ProductServices_Service;
import helper.ReviewState;

/**
 *
 * @author Magdelyn
 */
@ManagedBean
@RequestScoped
public class AddProductReviewManagedBean {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/ProductServices/ProductServices.wsdl")
    private ProductServices_Service service;
    private ProductServices         port;
    
    Long productId = (long) 2;
    String userEmail = "123@gmail.com";
    String comment;
    Integer rating;
    private ReviewState newReview = new ReviewState();

    public AddProductReviewManagedBean() {}
    
    @PostConstruct
    public void prepareService() {
        this.port = service.getProductServicesPort();
    }
    
    public void addReview() {
        System.out.println("NPMB:addReview is called");
        port.addReview(productId, userEmail, comment, rating);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Add Comment Sucessful", null));
    }

    // Getters and Setters
    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public ProductServices_Service getService() {
        return service;
    }

    public void setService(ProductServices_Service service) {
        this.service = service;
    }

    public ProductServices getPort() {
        return port;
    }

    public void setPort(ProductServices port) {
        this.port = port;
    }

    public ReviewState getNewReview() {
        return newReview;
    }

    public void setNewReview(ReviewState newReview) {
        this.newReview = newReview;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}
