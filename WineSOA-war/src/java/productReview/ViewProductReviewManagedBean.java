/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package productReview;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.xml.ws.WebServiceRef;
import webServices.ProductServices.ProductServices;
import webServices.ProductServices.ProductServices_Service;
import helper.ReviewState;

/**
 *
 * @author Magdelyn
 */
@ManagedBean
@ViewScoped
public class ViewProductReviewManagedBean {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/ProductServices/ProductServices.wsdl")
    private ProductServices_Service service;
    private ProductServices         port;
    
    Long productId = (long) 2;
    private List<ReviewState> reviewList;
    
    public ViewProductReviewManagedBean() {}
    
    @PostConstruct
    public void prepareService() {
        this.port = service.getProductServicesPort();
    }
    
    public List<ReviewState> viewReviews() {
        System.out.println("NPMB:viewReviews is called");
//        String gsonString = port.getReviews(productId);
//        reviewList = new Gson().fromJson(gsonString, new TypeToken<List<ReviewState>>() {
//        }.getType());
        System.out.println("review list:" + reviewList);
        return reviewList;
    }

    // Getters and Setters
    public void setReviewList(List<ReviewState> reviewList) {
        this.reviewList = reviewList;
    }

    public List<ReviewState> getReviewList() {
        return reviewList;
    }

    public ProductServices_Service getService() {
        return service;
    }

    public void setService(ProductServices_Service service) {
        this.service = service;
    }

    public ProductServices getPort() {
        return port;
    }

    public void setPort(ProductServices port) {
        this.port = port;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getProductId() {
        return productId;
    }
}
