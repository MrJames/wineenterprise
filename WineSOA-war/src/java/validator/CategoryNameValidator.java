/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package validator;

import commonBeans.CommonInfo;
import helper.CategoryState;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author JamesTran
 */
@Named(value = "categoryNameValidator")
@RequestScoped
public class CategoryNameValidator implements Validator {
    @Inject
    private CommonInfo commonInfo;
    
    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        Integer categoryType = (Integer) component.getAttributes().get("categoryType");
        for (CategoryState cs : commonInfo.getCategories()) {
            if (cs.getName().equals(value) && cs.getType() == categoryType) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "A category of this type with this name already exists.");
                throw new ValidatorException(msg);
            }
        }
    }
    
    // Getters and Setters
    public CommonInfo getCommonInfo() {
        return commonInfo;
    }

    public void setCommonInfo(CommonInfo commonInfo) {
        this.commonInfo = commonInfo;
    }
}
