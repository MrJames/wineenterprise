/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package validator;

import commonBeans.UserSessionBean;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.ws.WebServiceRef;
import webServices.AccountServices.AccountServices;
import webServices.AccountServices.AccountServices_Service;
/**
 *
 * @author James
 */
@Named(value = "passwordValidator")
@RequestScoped
public class PasswordValidator implements Validator {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/AccountServices/AccountServices.wsdl")
    private AccountServices_Service service;
    private AccountServices         port;
    @Inject
    private UserSessionBean userSession;
    
    public PasswordValidator() {}
    
    @PostConstruct
    public void prepareService() {
        this.port = service.getAccountServicesPort();
    }
    
    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String password = (String) value;
        if (!port.validatePassword(userSession.getUs().getEmail(), password)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "Oops! You have entered wrong password.");
            throw new ValidatorException(msg);
        }
    }
    
    
    // Getters and Setters
    public AccountServices_Service getService() {
        return service;
    }

    public void setService(AccountServices_Service service) {
        this.service = service;
    }

    public AccountServices getPort() {
        return port;
    }

    public void setPort(AccountServices port) {
        this.port = port;
    }

    public UserSessionBean getUserSession() {
        return userSession;
    }

    public void setUserSession(UserSessionBean userSession) {
        this.userSession = userSession;
    }
}
