/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package memberBeans;

import com.google.gson.Gson;
import helper.CategoryState;
import helper.ProductState;
import helper.ProductType;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.xml.ws.WebServiceRef;
import utilities.ServiceConstants;
import webServices.ProductServices.ProductServices;
import webServices.ProductServices.ProductServices_Service;

/**
 *
 * @author JamesTran
 */
@ManagedBean(name = "viewProduct")
@ViewScoped
public class ViewProduct {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/ProductServices/ProductServices.wsdl")
    private ProductServices_Service service;
    private ProductServices         port;
    
    private Long          productID;
    private ProductState  product;
    private CategoryState brand;
    private CategoryState region;
    private CategoryState flavor;
    private ProductType   type;
    
    public ViewProduct() {}
    
    @PostConstruct
    public void prepareService() {
        this.port = service.getProductServicesPort();
        String idStr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("productID");
        if (idStr != null) {
            this.productID = Long.valueOf(idStr);
            String jsonizedProduct = port.getProductDetails(productID);
            if (jsonizedProduct == null) {
                this.product = null;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                        "Failed", "The product ID is not valid."));

            } else {
                this.product = new Gson().fromJson(jsonizedProduct, ProductState.class);
                for (CategoryState cs : product.getCategories()) {
                    switch (cs.getType()) {
                        case ServiceConstants.CATEGORY_TYPE_BRAND:
                            this.brand    = cs;
                            break;
                        case ServiceConstants.CATEGORY_TYPE_REGION:
                            this.region   = cs;
                            break;
                        case ServiceConstants.CATEGORY_TYPE_FLAVOR:
                            this.flavor = cs;
                            break;
                    }
                }

                switch (product.getType()) {
                    case ServiceConstants.PRODUCT_TYPE_WINE:
                        this.type = new ProductType(ServiceConstants.PRODUCT_TYPE_WINE, "Wine");
                        break;
                    case ServiceConstants.PRODUCT_TYPE_COMPLEMENTARY:
                        this.type = new ProductType(ServiceConstants.PRODUCT_TYPE_COMPLEMENTARY, "Complementary");
                        break;
                }

            }
            
        } else {
            this.product = null;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                    "Failed", "The product ID is not valid."));
        }
        
    }
    
    // Getters and Setters
    public ProductServices_Service getService() {
        return service;
    }

    public void setService(ProductServices_Service service) {
        this.service = service;
    }

    public CategoryState getBrand() {
        return brand;
    }

    public void setBrand(CategoryState brand) {
        this.brand = brand;
    }

    public CategoryState getRegion() {
        return region;
    }

    public void setRegion(CategoryState region) {
        this.region = region;
    }

    public CategoryState getFlavor() {
        return flavor;
    }

    public void setFlavor(CategoryState flavor) {
        this.flavor = flavor;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }

    public Long getProductID() {
        return productID;
    }

    public void setProductID(Long productID) {
        this.productID = productID;
    }

    public ProductState getProduct() {
        return product;
    }

    public void setProduct(ProductState product) {
        this.product = product;
    }

    public ProductServices getPort() {
        return port;
    }

    public void setPort(ProductServices port) {
        this.port = port;
    }
}
