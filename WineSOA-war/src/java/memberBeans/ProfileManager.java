/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package memberBeans;

import com.google.gson.Gson;
import commonBeans.UserSessionBean;
import helper.UserState;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.xml.ws.WebServiceRef;
import utilities.ServiceConstants;
import webServices.AccountServices.AccountServices;
import webServices.AccountServices.AccountServices_Service;

/**
 *
 * @author JamesTran
 */
@ManagedBean(name = "profileManager")
@ViewScoped
public class ProfileManager {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/AccountServices/AccountServices.wsdl")
    private AccountServices_Service service;
    private AccountServices         port;
    private ResourceBundle          rb;
    @Inject
    private UserSessionBean userSession;
    private UserState       contactInfo;
    /**
     * Creates a new instance of ProfileManager
     */
    public ProfileManager() {}
    
    @PostConstruct
    public void prepareService() {
        this.rb   = ResourceBundle.getBundle("utilities.constants");
        this.port = service.getAccountServicesPort();
        this.contactInfo = new Gson().fromJson(port.getContactInfo(userSession.getUs().getEmail()), UserState.class);
    }
    
    public void updateContactInfo() {
        String jsonizedObject = new Gson().toJson(contactInfo);
        int result = port.updateContactInfo(jsonizedObject);
        
        if (result == ServiceConstants.STATUS_SUCCESSFUL) FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_INFO, "Successful", "You have successfully updated your Contact info."));
    }
    
    // Getters and Setters
    public AccountServices_Service getService() {
        return service;
    }

    public void setService(AccountServices_Service service) {
        this.service = service;
    }

    public AccountServices getPort() {
        return port;
    }

    public void setPort(AccountServices port) {
        this.port = port;
    }

    public ResourceBundle getRb() {
        return rb;
    }

    public void setRb(ResourceBundle rb) {
        this.rb = rb;
    }

    public UserState getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(UserState contactInfo) {
        this.contactInfo = contactInfo;
    }
}
