/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package memberBeans;

import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.inject.Named;
//import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.xml.ws.WebServiceRef;
import utilities.ServiceConstants;
import utilities.WebConstants;
import webServices.AccountServices.AccountServices;
import webServices.AccountServices.AccountServices_Service;

/**
 *
 * @author JamesTran
 */
@ManagedBean(name = "activateMember")
@RequestScoped
public class ActivateAccount {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/AccountServices/AccountServices.wsdl")
    private AccountServices_Service service;
    private AccountServices         port;
    private ResourceBundle          rb;
    @ManagedProperty(value = "#{param.id}")
    private String id;
    
    /**
     * Creates a new instance of ActivateAccount
     */
    public ActivateAccount() {}
    
    @PostConstruct
    public void prepareService() {
        this.rb   = ResourceBundle.getBundle("utilities.constants");
        this.port = service.getAccountServicesPort();
        if (id == null || (port.activateMember(id) == ServiceConstants.STATUS_NOT_FOUND)) FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_WARN, "Failed", "Oops! Your activation link is invalid.<br/><br/>Please double-check your Activation link or click <a href=\"" + WebConstants.SYSTEM_MEMBER_SSL_PATH + "RegisterAccount.xhtml\">here</a> to re-register your Lingua123 account."));
        else FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_INFO, "Successful", "You have successfully activated your WineExpress account.<br/><br/>Please click <a href=\"./index.xhtml\">here</a> to return to WineExpress “Home” page."));
    }
    
    // Getters and Setters
    public AccountServices_Service getService() {
        return service;
    }

    public void setService(AccountServices_Service service) {
        this.service = service;
    }

    public AccountServices getPort() {
        return port;
    }

    public void setPort(AccountServices port) {
        this.port = port;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
