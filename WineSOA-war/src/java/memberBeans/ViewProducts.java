/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package memberBeans;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import commonBeans.CommonInfo;
import helper.ProductState;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.xml.ws.WebServiceRef;
import org.primefaces.model.TreeNode;
import webServices.ProductServices.ProductServices;
import webServices.ProductServices.ProductServices_Service;

/**
 *
 * @author JamesTran
 */
@ManagedBean(name = "viewProducts")
@ViewScoped
public class ViewProducts {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/ProductServices/ProductServices.wsdl")
    private ProductServices_Service service;
    private ProductServices         port;
    @Inject
    private CommonInfo commonInfo;
    
    private List<ProductState> products;
    private List<ProductState> filteredProducts;
    private TreeNode           root;
    private Long               categoryID;
    
    public ViewProducts() {}
    
    @PostConstruct
    public void prepareService() {
        this.port     = service.getProductServicesPort();
        this.products = new Gson().fromJson(port.getAllProducts(), new TypeToken<List<ProductState>>(){}.getType());
        this.filteredProducts = new LinkedList();
        this.filteredProducts.addAll(products);
    }
    
    public void filterProductsByType(Integer type) {
        this.filteredProducts = new LinkedList();
        this.filteredProducts.addAll(products);
        
        // Filter by product type
        if (type != -1) {
            Iterator<ProductState> iter = filteredProducts.iterator();
            while (iter.hasNext()) {
                ProductState ps = iter.next();
                 if (ps.getType() != type) iter.remove();
            }
        }
    }
    
    // Getters and Setters
    public ProductServices_Service getService() {
        return service;
    }

    public void setService(ProductServices_Service service) {
        this.service = service;
    }

    public Long getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Long categoryID) {
        this.categoryID = categoryID;
    }

    public CommonInfo getCommonInfo() {
        return commonInfo;
    }

    public void setCommonInfo(CommonInfo commonInfo) {
        this.commonInfo = commonInfo;
    }

    public List<ProductState> getFilteredProducts() {
        return filteredProducts;
    }

    public void setFilteredProducts(List<ProductState> filteredProducts) {
        this.filteredProducts = filteredProducts;
    }

    public TreeNode getRoot() {
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public ProductServices getPort() {
        return port;
    }

    public void setPort(ProductServices port) {
        this.port = port;
    }

    public List<ProductState> getProducts() {
        return products;
    }

    public void setProducts(List<ProductState> products) {
        this.products = products;
    }
}
