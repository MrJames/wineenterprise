/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package memberBeans;

import com.google.gson.Gson;
import commonBeans.UserSessionBean;
import helper.UserState;
import java.io.IOException;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceRef;
import utilities.WebConstants;
import webServices.AccountServices.AccountServices;
import webServices.AccountServices.AccountServices_Service;

/**
 *
 * @author JamesTran
 */
@Named(value = "loginBean")
@RequestScoped
public class Login {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/AccountServices/AccountServices.wsdl")
    private AccountServices_Service service;
    private AccountServices         port;
    private ResourceBundle          rb;
    @Inject
    private UserSessionBean userSession;
    
    private String email;
    private String password;
    /**
     * Creates a new instance of Login
     */
    public Login() {}
    
    @PostConstruct
    public void prepareService() {
        this.rb   = ResourceBundle.getBundle("utilities.constants");
        this.port = service.getAccountServicesPort();
        
        if (!FacesContext.getCurrentInstance().isPostback()) {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            this.email = request.getParameter("email");
            this.password = request.getParameter("password");

            if (email == null || email.isEmpty() || password == null || password.isEmpty()) FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "Oops! Please enter both Email and Password."));
            else {
                String jsonizedObject = port.login1(email, password);
                if (jsonizedObject == null) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "Oops! Your Email or Password is not correct."));

                } else {
                    UserState us = new Gson().fromJson(jsonizedObject, UserState.class);
                    
                    if (!us.isActivated())   FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "Oops! Your account has not been activated yet.<br/><br/>Please check your mail box for our Activation email."));
                    else if (us.isBlocked()) FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "Oops! Your account has been blocked by our admin.<br/><br/>Please click <a href=\"" + WebConstants.SYSTEM_MEMBER_PATH + "ContactUs.xhtml\">here</a> to contact us if you don't know why you are blocked."));
                    else if (us.isDeleted()) FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "Oops! This account has been deleted."));
                    else {
                        try {
                            userSession.setUs(us);
                            
                            if (us.isIsAdmin()) {
                                userSession.saveAdminSession(request);
                                FacesContext.getCurrentInstance().getExternalContext().redirect(rb.getString("SYSTEM_ADMIN_PATH") + "AdminArea.xhtml");
                            
                            } else {
                                userSession.saveMemberSession(request);
                                FacesContext.getCurrentInstance().getExternalContext().redirect(WebConstants.SYSTEM_MEMBER_PATH + "MemberArea.xhtml");
                            }

                        } catch (IOException ex) {
                            System.out.println("IOException: " + ex.toString());
                        }
                    }
                } 
            }
        }
    }
    
    public void logIn() {
        if (email == null || email.isEmpty() || password == null || password.isEmpty()) FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "Oops! Please enter both Email and Password."));
        else {
            String jsonizedObject = port.login1(email, password);
            if (jsonizedObject == null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "Oops! Your Email or Password is not correct."));
            
            } else {
                UserState us = new Gson().fromJson(jsonizedObject, UserState.class);
                
                if (!us.isActivated())   FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "Oops! Your account has not been activated yet.<br/><br/>Please check your mail box for our Activation email."));
                else if (us.isBlocked()) FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "Oops! Your account has been blocked by our admin.<br/><br/>Please click <a href=\"" + WebConstants.SYSTEM_MEMBER_PATH + "ContactUs.xhtml\">here</a> to contact us if you don't know why you are blocked."));
                else if (us.isDeleted()) FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "Oops! This account has been deleted."));
                else {
                    try {
                        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
                        userSession.setUs(us);
                        
                        if (us.isIsAdmin()) {
                            userSession.saveAdminSession(request);
                            FacesContext.getCurrentInstance().getExternalContext().redirect(rb.getString("SYSTEM_ADMIN_PATH") + "AdminArea.xhtml");

                        } else {
                            userSession.saveMemberSession(request);
                            FacesContext.getCurrentInstance().getExternalContext().redirect(WebConstants.SYSTEM_MEMBER_PATH + "MemberArea.xhtml");
                        }
                    } catch (IOException ex) {
                        System.out.println("IOException: " + ex.toString());
                    }
                }
            } 
        }
    }
    
    public void logOut() {
        try {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            userSession.invalidateSession(request);
            
            FacesContext.getCurrentInstance().getExternalContext().redirect(WebConstants.SYSTEM_MEMBER_PATH + "index.xhtml");
        } catch (IOException ex) {
            System.out.println("IOException: " + ex.toString());
        }
    }
    
    // Getters and Setters
    public AccountServices_Service getService() {
        return service;
    }

    public void setService(AccountServices_Service service) {
        this.service = service;
    }

    public AccountServices getPort() {
        return port;
    }

    public void setPort(AccountServices port) {
        this.port = port;
    }

    public ResourceBundle getRb() {
        return rb;
    }

    public void setRb(ResourceBundle rb) {
        this.rb = rb;
    }

    public UserSessionBean getUserSession() {
        return userSession;
    }

    public void setUserSession(UserSessionBean userSession) {
        this.userSession = userSession;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
