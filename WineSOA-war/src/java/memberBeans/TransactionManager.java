/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package memberBeans;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import commonBeans.UserSessionBean;
import helper.ProductState;
import helper.TransactionsState;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.xml.ws.WebServiceRef;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import utilities.ServiceConstants;
import webServices.AccountServices.AccountServices;
import webServices.AccountServices.AccountServices_Service;
import webServices.ProductServices.ProductServices;
import webServices.ProductServices.ProductServices_Service;

/**
 *
 * @author JamesTran
 */
@ManagedBean(name = "transactionManager")
@ViewScoped
public class TransactionManager {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/AccountServices/AccountServices.wsdl")
    private AccountServices_Service accountService;
    private AccountServices         accountPort;
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/ProductServices/ProductServices.wsdl")
    private ProductServices_Service productService;
    private ProductServices         productPort;
    @Inject
    private UserSessionBean userSession;
    
    private Integer           rating;
    private TransactionsState transaction;
    private ProductState      product;
    private LazyDataModel<TransactionsState> transactions;
    
    public TransactionManager() { }
    
    @PostConstruct
    public void init() {
        this.accountPort  = accountService.getAccountServicesPort();
        this.productPort  = productService.getProductServicesPort();
        this.transactions = new LazyDataModel() {

            @Override
            public List<TransactionsState> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map filters) {
                String jsonizedTransactions = accountPort.loadTransactionHistory(userSession.getUs().getEmail(), first, pageSize);
                return new Gson().fromJson(jsonizedTransactions, new TypeToken<List<TransactionsState>>(){}.getType());
            }
            
        };
        
        this.transactions.setRowCount(accountPort.getNumberOfTransactions(userSession.getUs().getEmail()));
    }
    
    public void rate() {
        int result = this.productPort.rateProduct(transaction.getId(), product.getId(), userSession.getUs().getEmail(), rating);
        
        if (result == ServiceConstants.STATUS_NOT_FOUND) {
            FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    "Failed", "Some information does not exist."));
        
        } else if (result == ServiceConstants.STATUS_ILLEGAL_STATE) {
            FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    "Failed", "You have been blocked."));
       
        } else if (result == ServiceConstants.STATUS_FAILED) {
            FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    "Failed", "You can only rate this item after buying it."));
        
        } else {
            FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_INFO, 
                    "Successful", "You have successfully rated the product."));
        }
    }
    
    // Getters and Setters
    public UserSessionBean getUserSession() {
        return userSession;
    }

    public void setUserSession(UserSessionBean userSession) {
        this.userSession = userSession;
    }

    public AccountServices_Service getAccountService() {
        return accountService;
    }

    public void setAccountService(AccountServices_Service accountService) {
        this.accountService = accountService;
    }

    public AccountServices getAccountPort() {
        return accountPort;
    }

    public void setAccountPort(AccountServices accountPort) {
        this.accountPort = accountPort;
    }

    public ProductServices_Service getProductService() {
        return productService;
    }

    public void setProductService(ProductServices_Service productService) {
        this.productService = productService;
    }

    public ProductServices getProductPort() {
        return productPort;
    }

    public void setProductPort(ProductServices productPort) {
        this.productPort = productPort;
    }

    public TransactionsState getTransaction() {
        return transaction;
    }

    public void setTransaction(TransactionsState transaction) {
        this.transaction = transaction;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public ProductState getProduct() {
        return product;
    }

    public void setProduct(ProductState product) {
        this.product = product;
    }

    public LazyDataModel<TransactionsState> getTransactions() {
        return transactions;
    }

    public void setTransactions(LazyDataModel<TransactionsState> transactions) {
        this.transactions = transactions;
    }
}
