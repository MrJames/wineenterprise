/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package memberBeans;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;
import javax.xml.ws.WebServiceRef;
import utilities.ServiceConstants;
import utilities.WebConstants;
import webServices.AccountServices.AccountServices;
import webServices.AccountServices.AccountServices_Service;

/**
 *
 * @author JamesTran
 */
@Named(value = "registerMember")
@RequestScoped
public class RegisterAccount {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/AccountServices/AccountServices.wsdl")
    private AccountServices_Service service;
    private AccountServices         port;

    private String  email;
    private String  password;
    private String  firstName;
    private String  lastName;
    private String  gender;
    private String  countryCode;
    private String  phone;
    private boolean agreedToTerms;
    /**
     * Creates a new instance of RegisterAccount
     */
    public RegisterAccount() {}
    
    @PostConstruct
    public void prepareService() {
        this.port = service.getAccountServicesPort();
    }
    
    public void validateEmail(FacesContext context, UIComponent toValidate,
            Object value) throws ValidatorException {
        
        String emailStr = (String) value;
        if (emailStr.indexOf("@") == -1) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "This email is invalid.");
            throw new ValidatorException(message);
            
        } else if (port.checkEmailExistence(emailStr) == ServiceConstants.STATUS_NOT_UNIQUE) {
            FacesMessage message = new FacesMessage("This email has already been registered.");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }
    }
    
    public void validateTerms(FacesContext context, UIComponent toValidate,
            Object value) throws ValidatorException {
        
        Boolean accepted = (Boolean) value;
        if (!accepted) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "You need to accept our Terms and Conditions.");
            throw new ValidatorException(message);
            
        } 
    }
    
    public void register() {
        int result = port.registerMember1(email, password, gender, firstName, lastName, countryCode, phone, WebConstants.SYSTEM_MEMBER_PATH + "ActivateAccount.xhtml");
        
        if (result == ServiceConstants.STATUS_NOT_UNIQUE) FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Not unique", "Oops! This email has already been registered. Please click <a href=\"" + WebConstants.SYSTEM_MEMBER_PATH + "AccountRecovery.xhtml\">here</a> if you forgot your password."));
        else if (result == ServiceConstants.STATUS_SUCCESSFUL) FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Successful", "You have successfuly registered as a member of WineExpress. An account activation email has been sent to " + email + ".<br/><br/>Please remember you need to activate your account before you can log in to WineExpress website. To activate, please click on the activation link provided in your email.")); 
    }
    
    // Getters and Setters
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isAgreedToTerms() {
        return agreedToTerms;
    }

    public void setAgreedToTerms(boolean agreedToTerms) {
        this.agreedToTerms = agreedToTerms;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
