/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package memberBeans;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import helper.CategoryState;
import helper.ProductState;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceRef;
import webServices.ProductServices.ProductServices;
import webServices.ProductServices.ProductServices_Service;

/**
 *
 * @author JamesTran
 */
@ManagedBean(name = "searchBean")
@ViewScoped
public class SearchBean {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/ProductServices/ProductServices.wsdl")
    private ProductServices_Service service;
    private ProductServices         port;
    private List<ProductState>      products;
    private List<ProductState>      filteredProducts;
    
    /**
     * Creates a new instance of SearchBean
     */
    public SearchBean() {}
    
    @PostConstruct
    public void prepareService() {
        this.port = service.getProductServicesPort();
        
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        if (request.getParameter("byCategory") == null) {
            String keyWord = request.getParameter("keyWord");
            
            if (keyWord == null) {
                this.products = new Gson().fromJson(port.getAllProducts(), new TypeToken<List<ProductState>>(){}.getType());
                this.filteredProducts = new LinkedList();
                this.filteredProducts.addAll(products);
            } else this.searchByKeyWord(keyWord);
                
        } else this.searchByCategory(request.getParameter("brand"), request.getParameter("region"), request.getParameter("flavor"));
    }
    
    public void searchByKeyWord(String keyWord) {
        this.products = new Gson().fromJson(port.searchProduct(keyWord), new TypeToken<List<ProductState>>(){}.getType());
        this.filteredProducts = new LinkedList();
        this.filteredProducts.addAll(products);
    }
    
    public void searchByCategory(String brandID, String regionID, String flavorID) {
        // Prepare to filter
        this.products = new Gson().fromJson(port.getAllProducts(), new TypeToken<List<ProductState>>(){}.getType());
        Iterator<ProductState> iter = products.iterator();
        
        // Filter by brand
        if (Long.valueOf(brandID) != 0) {
            CategoryState cs = new CategoryState(Long.valueOf(brandID));
            while (iter.hasNext()) {
                ProductState p = iter.next();
                if (!p.getCategories().contains(cs)) iter.remove();
            }
        }
        
        // Filter by region
        if (Long.valueOf(regionID) != 0) {
            CategoryState cs = new CategoryState(Long.valueOf(regionID));
            while (iter.hasNext()) {
                ProductState p = iter.next();
                if (!p.getCategories().contains(cs)) iter.remove();
            }
        }
        
        // Filter by flavor
        if (Long.valueOf(flavorID) != 0) {
            CategoryState cs = new CategoryState(Long.valueOf(flavorID));
            while (iter.hasNext()) {
                ProductState p = iter.next();
                if (!p.getCategories().contains(cs)) iter.remove();
            }
        }
        
        // Prepare result
        this.filteredProducts = new LinkedList();
        this.filteredProducts.addAll(products);
    }
    
    public void filterProductsByType(Integer type) {
        this.filteredProducts = new LinkedList();
        this.filteredProducts.addAll(products);
        
        System.out.println("FILTER TYPE: " + type);
        // Filter by product type
        if (type != -1) {
            Iterator<ProductState> iter = filteredProducts.iterator();
            while (iter.hasNext()) {
                ProductState ps = iter.next();
                 if (ps.getType() != type) iter.remove();
            }
        }
    }
    
    // Getters and Setters
    public List<ProductState> getProducts() {
        return products;
    }

    public void setProducts(List<ProductState> products) {
        this.products = products;
    }

    public List<ProductState> getFilteredProducts() {
        return filteredProducts;
    }

    public void setFilteredProducts(List<ProductState> filteredProducts) {
        this.filteredProducts = filteredProducts;
    }

    public ProductServices_Service getService() {
        return service;
    }

    public void setService(ProductServices_Service service) {
        this.service = service;
    }

    public ProductServices getPort() {
        return port;
    }

    public void setPort(ProductServices port) {
        this.port = port;
    }
}
