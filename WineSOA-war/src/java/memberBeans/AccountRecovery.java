/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package memberBeans;

import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.xml.ws.WebServiceRef;
import utilities.ServiceConstants;
import utilities.WebConstants;
import webServices.AccountServices.AccountServices;
import webServices.AccountServices.AccountServices_Service;

/**
 *
 * @author JamesTran
 */
@Named(value = "accountRecovery")
@RequestScoped
public class AccountRecovery {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/AccountServices/AccountServices.wsdl")
    private AccountServices_Service service;
    private AccountServices         port;
    private ResourceBundle          rb;
    private String                  email;
    
    /**
     * Creates a new instance of accountRecovery
     */
    public AccountRecovery() {}
    
    @PostConstruct
    public void prepareService() {
        this.rb   = ResourceBundle.getBundle("utilities.constants");
        this.port = service.getAccountServicesPort();
    }
    
    public void recoverPassword() {
        int result = port.recoverPassword(email);
        
        if (result == ServiceConstants.STATUS_NOT_FOUND) FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "Oops! We are unable to find your email address in our system.  Please click <a href=\"" + WebConstants.SYSTEM_MEMBER_SSL_PATH + "RegisterAccount.xhtml\">here</a> to register as a member at WineExpress or click the OK button to return to Home page."));
        else FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_INFO, "Successful", "Your new password has been sent to this email address: <EMAIL>.<br/<br/>Please proceed to change your password as soon as possible.".replaceFirst("<EMAIL>", email)));
    }
    
    // Getters and Setters
    public AccountServices_Service getService() {
        return service;
    }

    public void setService(AccountServices_Service service) {
        this.service = service;
    }

    public AccountServices getPort() {
        return port;
    }

    public void setPort(AccountServices port) {
        this.port = port;
    }

    public ResourceBundle getRb() {
        return rb;
    }

    public void setRb(ResourceBundle rb) {
        this.rb = rb;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
