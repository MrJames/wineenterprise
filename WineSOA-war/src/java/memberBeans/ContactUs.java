/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package memberBeans;

import com.google.gson.Gson;
import helper.CategoryState;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.xml.ws.WebServiceRef;
import webServices.AccountServices.AccountServices;
import webServices.AccountServices.AccountServices_Service;
import utilities.ServiceConstants;

/**
 *
 * @author JamesTran
 */
@Named(value = "contactUs")
@RequestScoped
public class ContactUs {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/AccountServices/AccountServices.wsdl")
    private AccountServices_Service service;
    private AccountServices         port;
    
    private String name;
    private String email;
    private String subject;
    private String enquiry;
    
    public ContactUs() {}
    
    @PostConstruct
    public void prepareService() {
        this.port = service.getAccountServicesPort();
    }
    
    public void validateEmail(FacesContext context, UIComponent toValidate,
            Object value) throws ValidatorException {
        
        String emailStr = (String) value;
        if (emailStr.indexOf("@") == -1) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "This email is invalid.");
            throw new ValidatorException(message);
            
        } 
    }
    
    public void submitEnquiry() {
        int result = port.submitEnquiry(name, email, subject, enquiry);
        
        if (result == ServiceConstants.STATUS_UNEXPECTED_ERROR) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Failed", "Unexpected error happened. Please try again later!"));

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                    "Successful", "You have successfuly submitted an enquiry."));
            
        }
    }
     
    // Getters and Setters
    public AccountServices_Service getService() {
        return service;
    }

    public void setService(AccountServices_Service service) {
        this.service = service;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public AccountServices getPort() {
        return port;
    }

    public void setPort(AccountServices port) {
        this.port = port;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEnquiry() {
        return enquiry;
    }

    public void setEnquiry(String enquiry) {
        this.enquiry = enquiry;
    }
}
