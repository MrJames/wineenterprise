/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package memberBeans;

import commonBeans.UserSessionBean;
import java.io.IOException;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceRef;
import utilities.ServiceConstants;
import utilities.WebConstants;
import webServices.AccountServices.AccountServices;
import webServices.AccountServices.AccountServices_Service;

/**
 *
 * @author JamesTran
 */
@Named(value = "accountSettings")
@RequestScoped
public class AccountSettings {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/AccountServices/AccountServices.wsdl")
    private AccountServices_Service service;
    private AccountServices         port;
    private ResourceBundle          rb;
    @Inject
    private UserSessionBean userSession;
    private String currentPassword;
    private String newPassword;
    
    /**
     * Creates a new instance of AccountSettings
     */
    public AccountSettings() {}
    
    @PostConstruct
    public void prepareService() {
        this.rb   = ResourceBundle.getBundle("utilities.constants");
        this.port = service.getAccountServicesPort();
    }
    
    public void changePassword() {
        int result = port.changePassword(userSession.getUs().getEmail(), currentPassword, newPassword);
        
        if (result == ServiceConstants.STATUS_WRONG_PASSWORD) FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "Oops! You have entered wrong password."));
        else if (result == ServiceConstants.STATUS_SUCCESSFUL) FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_INFO, "Successful", "You have successfully changed your password."));
    }
    
    public void cancelAccount() {
        int result = port.cancelAccount(userSession.getUs().getEmail(), currentPassword);
        
        if (result == ServiceConstants.STATUS_WRONG_PASSWORD) FacesContext.getCurrentInstance().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "Oops! You have entered wrong password."));
        else if (result == ServiceConstants.STATUS_SUCCESSFUL) {
            try {
                HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
                userSession.invalidateSession(request);

                FacesContext.getCurrentInstance().getExternalContext().redirect(WebConstants.SYSTEM_MEMBER_PATH + "index.xhtml");
            } catch (IOException ex) {
                System.out.println("IOException: " + ex.toString());
            }
        }
    }
    
    // Getters and Setterss
    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public AccountServices_Service getService() {
        return service;
    }

    public void setService(AccountServices_Service service) {
        this.service = service;
    }

    public AccountServices getPort() {
        return port;
    }

    public void setPort(AccountServices port) {
        this.port = port;
    }

    public UserSessionBean getUserSession() {
        return userSession;
    }

    public void setUserSession(UserSessionBean userSession) {
        this.userSession = userSession;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
