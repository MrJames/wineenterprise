/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package memberBeans;

import com.google.gson.Gson;
import helper.TransactionItemState;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceRef;
import utilities.WebConstants;
import webServices.ProductServices.ProductServices;
import webServices.ProductServices.ProductServices_Service;

/**
 *
 * @author JamesTran
 */
@ManagedBean(name = "confirmOrder")
@RequestScoped
public class ConfirmOrder {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/ec2-46-137-247-126.ap-southeast-1.compute.amazonaws.com/ProductServices/ProductServices.wsdl")
    private ProductServices_Service service;
    private ProductServices         port;
    
    // Assign posted variables to local variables
    @ManagedProperty(value = "#{param.custom}")
    private String customerEmail;
    @ManagedProperty(value = "#{param.mc_gross}")
    private double amountPaid;
    @ManagedProperty(value = "#{param.payment_status}")
    private String paymentStatus;
    @ManagedProperty(value = "#{param.mc_currency}")
    private String paymentCurrency;
    @ManagedProperty(value = "#{param.txn_id}")
    private String paypalID;
    @ManagedProperty(value = "#{param.receiver_email}")
    private String businessEmail;
    
    public ConfirmOrder() { }
    
    @PostConstruct
    public void confirmOrder() {
        try {
            System.out.println("CONFIRM ORDER: " + paypalID);
            
            this.port = service.getProductServicesPort();
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            // Read post from PayPal system and add 'cmd'
            Enumeration en = request.getParameterNames();
            String str = "cmd=_notify-validate";
            while (en.hasMoreElements()) {
                try {
                    String paramName = (String) en.nextElement();
                    String paramValue = request.getParameter(paramName);
                    str = str + "&" + paramName + "=" + URLEncoder.encode(paramValue, "UTF-8");
                    
                } catch (UnsupportedEncodingException ex) {
                    System.out.println("ConfirmOrder - UnsupportedEncodingException: " + ex.toString());
                }
            }

            // Post back to PayPal system to validate
            // NOTE: change http: to https: in the following URL to verify using SSL (for increased security).
            URL u = new URL("https://www.sandbox.paypal.com/cgi-bin/webscr");
            URLConnection uc = u.openConnection();
            uc.setDoOutput(true);
            uc.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
            PrintWriter pw = new PrintWriter(uc.getOutputStream());
            pw.println(str);
            pw.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
            String result = in.readLine();
            in.close();
    
            // Check notification validation
            if (result.equals("VERIFIED")) {
                
                if (businessEmail.equals(WebConstants.SYSTEM_PAYPAL_BUSINESS_EMAIL)) {
                    if (paymentStatus.equals("Completed")) {
//                        // Check that paymentStatus=Completed
//                        // Check that txnId has not been previously processed
//                        // Check that receiverEmail is your Primary PayPal email
//                        // Check that paymentAmount/paymentCurrency are correct
//                        // process payment
                        
                        Set<TransactionItemState> items = new HashSet();
                        int i = 1;
                        while (request.getParameter("item_number" + i) != null) {
                            Long id       = Long.valueOf(request.getParameter("item_number" + i));
                            int  quantity = Integer.valueOf(request.getParameter("quantity" + i));
                            TransactionItemState item = new TransactionItemState(id, quantity);
                            items.add(item);
                            i++;
                        }
                        
                        // Process the transaction
                        String jsonizedItems = new Gson().toJson(items);
                        port.confirmOrder(customerEmail, paypalID, jsonizedItems, amountPaid);
                            
                    } else if (paymentStatus.equals("Denied")) {
                        //send email to customer  // TODO: check if need to change status returned
                        
                    } else if (paymentStatus.equals("Pending")) {
                        // TODO: refund if needed
                        
                    } else if (paymentStatus.equals("Reversed")) {
                        // cancel classes and send email
                        
                    }
                }
                
            } else if (result.equals("INVALID")) {
                System.out.println("ConfirmOrder - INVALID");
            }
            
        } catch (IOException ex) {
            System.out.println("ConfirmOrder - IOException: " + ex.toString());
        }
    }
    
    // Getters and Setters
    public ProductServices_Service getService() {
        return service;
    }

    public void setService(ProductServices_Service service) {
        this.service = service;
    }

    public ProductServices getPort() {
        return port;
    }

    public void setPort(ProductServices port) {
        this.port = port;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public double getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(double amountPaid) {
        this.amountPaid = amountPaid;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentCurrency() {
        return paymentCurrency;
    }

    public void setPaymentCurrency(String paymentCurrency) {
        this.paymentCurrency = paymentCurrency;
    }

    public String getPaypalID() {
        return paypalID;
    }

    public void setPaypalID(String paypalID) {
        this.paypalID = paypalID;
    }

    public String getBusinessEmail() {
        return businessEmail;
    }

    public void setBusinessEmail(String businessEmail) {
        this.businessEmail = businessEmail;
    }
}
