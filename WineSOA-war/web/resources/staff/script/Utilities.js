function signOut()
{
    $.get("StfSignOut", window.location.replace("http://lingua123.com/alpha/staff/"));
}

function PopupCenter(pageURL,title,w,h)
{
    var left = (screen.width/2)-(w/2);
    var top = (screen.height/2)-(h/2);
    var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
    if (window.focus) {
        targetWin.focus()
    }
}

function showPage(thePage, location)
{
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (thePage == "StfLogin") addScript("Login.js");
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById(location).innerHTML=xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET",thePage,true);
    xmlhttp.send();
}

function addScript(scriptSource)
{
    var script = document.createElement('script');
    script.id = scriptSource;
    script.type = 'text/javascript';
    script.src = "../employee/script/" + scriptSource;
    var head = document.getElementsByTagName("head")[0];
    head.appendChild(script);
}

function focusIt(target)
{
    var myTarget = document.getElementById(target);
    myTarget.focus();
}